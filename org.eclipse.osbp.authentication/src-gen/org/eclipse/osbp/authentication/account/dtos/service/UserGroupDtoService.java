package org.eclipse.osbp.authentication.account.dtos.service;

import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;

@SuppressWarnings("all")
public class UserGroupDtoService extends AbstractDTOService<UserGroupDto, UserGroup> {
  public UserGroupDtoService() {
    // set the default persistence ID
    setPersistenceId("authentication");
  }
  
  public Class<UserGroupDto> getDtoClass() {
    return UserGroupDto.class;
  }
  
  public Class<UserGroup> getEntityClass() {
    return UserGroup.class;
  }
  
  public Object getId(final UserGroupDto dto) {
    return dto.getId();
  }
}
