package org.eclipse.osbp.authentication.account.dtos.mapper;

import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.account.entities.UserAccount;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link UserAccountDto} to and from the entity {@link UserAccount}.
 * 
 */
@SuppressWarnings("all")
public class UserAccountDtoMapper<DTO extends UserAccountDto, ENTITY extends UserAccount> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public UserAccount createEntity() {
    return new UserAccount();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public UserAccountDto createDto() {
    return new UserAccountDto();
  }
  
  /**
   * Maps the entity {@link UserAccount} to the dto {@link UserAccountDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final UserAccountDto dto, final UserAccount entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setEmail(toDto_email(entity, context));
    dto.setUserName(toDto_userName(entity, context));
    dto.setPassword(toDto_password(entity, context));
    dto.setExtraPassword(toDto_extraPassword(entity, context));
    dto.setUserGroup(toDto_userGroup(entity, context));
    dto.setPosition(toDto_position(entity, context));
    dto.setDefaultPerspective(toDto_defaultPerspective(entity, context));
    dto.setNotRegistered(toDto_notRegistered(entity, context));
    dto.setEnabled(toDto_enabled(entity, context));
    dto.setLocked(toDto_locked(entity, context));
    dto.setPasswordReset(toDto_passwordReset(entity, context));
    dto.setSuperuser(toDto_superuser(entity, context));
    dto.setForcePwdChange(toDto_forcePwdChange(entity, context));
    dto.setFailedAttempt(toDto_failedAttempt(entity, context));
    dto.setSuccessfulAttempt(toDto_successfulAttempt(entity, context));
    dto.setCookieHashCode(toDto_cookieHashCode(entity, context));
    dto.setLocaleTag(toDto_localeTag(entity, context));
    dto.setProfileimage(toDto_profileimage(entity, context));
    dto.setTheme(toDto_theme(entity, context));
    dto.setPrintService(toDto_printService(entity, context));
    dto.setSavedProperties(toDto_savedProperties(entity, context));
    dto.setDashBoard(toDto_dashBoard(entity, context));
    dto.setFavorites(toDto_favorites(entity, context));
    dto.setFilters(toDto_filters(entity, context));
  }
  
  /**
   * Maps the dto {@link UserAccountDto} to the entity {@link UserAccount}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final UserAccountDto dto, final UserAccount entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setEmail(toEntity_email(dto, entity, context));
    entity.setUserName(toEntity_userName(dto, entity, context));
    entity.setPassword(toEntity_password(dto, entity, context));
    entity.setExtraPassword(toEntity_extraPassword(dto, entity, context));
    entity.setUserGroup(toEntity_userGroup(dto, entity, context));
    entity.setPosition(toEntity_position(dto, entity, context));
    entity.setDefaultPerspective(toEntity_defaultPerspective(dto, entity, context));
    entity.setNotRegistered(toEntity_notRegistered(dto, entity, context));
    entity.setEnabled(toEntity_enabled(dto, entity, context));
    entity.setLocked(toEntity_locked(dto, entity, context));
    entity.setPasswordReset(toEntity_passwordReset(dto, entity, context));
    entity.setSuperuser(toEntity_superuser(dto, entity, context));
    entity.setForcePwdChange(toEntity_forcePwdChange(dto, entity, context));
    entity.setFailedAttempt(toEntity_failedAttempt(dto, entity, context));
    entity.setSuccessfulAttempt(toEntity_successfulAttempt(dto, entity, context));
    entity.setCookieHashCode(toEntity_cookieHashCode(dto, entity, context));
    entity.setLocaleTag(toEntity_localeTag(dto, entity, context));
    entity.setProfileimage(toEntity_profileimage(dto, entity, context));
    entity.setTheme(toEntity_theme(dto, entity, context));
    entity.setPrintService(toEntity_printService(dto, entity, context));
    entity.setSavedProperties(toEntity_savedProperties(dto, entity, context));
    entity.setDashBoard(toEntity_dashBoard(dto, entity, context));
    entity.setFavorites(toEntity_favorites(dto, entity, context));
    entity.setFilters(toEntity_filters(dto, entity, context));
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_id(final UserAccount in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_id(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property email from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_email(final UserAccount in, final MappingContext context) {
    return in.getEmail();
  }
  
  /**
   * Maps the property email from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_email(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getEmail();
  }
  
  /**
   * Maps the property userName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_userName(final UserAccount in, final MappingContext context) {
    return in.getUserName();
  }
  
  /**
   * Maps the property userName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_userName(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getUserName();
  }
  
  /**
   * Maps the property password from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_password(final UserAccount in, final MappingContext context) {
    return in.getPassword();
  }
  
  /**
   * Maps the property password from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_password(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getPassword();
  }
  
  /**
   * Maps the property extraPassword from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_extraPassword(final UserAccount in, final MappingContext context) {
    return in.getExtraPassword();
  }
  
  /**
   * Maps the property extraPassword from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_extraPassword(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getExtraPassword();
  }
  
  /**
   * Maps the property userGroup from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected UserGroupDto toDto_userGroup(final UserAccount in, final MappingContext context) {
    if(in.getUserGroup() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup>) getToDtoMapper(UserGroupDto.class, in.getUserGroup().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	UserGroupDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getUserGroup()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getUserGroup(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getUserGroup(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property userGroup from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected UserGroup toEntity_userGroup(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    if(in.getUserGroup() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup>) getToEntityMapper(in.getUserGroup().getClass(), UserGroup.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	UserGroup entity = null;
    	entity = context.get(mapper.createEntityHash(in.getUserGroup()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (UserGroup) context
    			.findEntityByEntityManager(UserGroup.class, in.getUserGroup().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getUserGroup()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getUserGroup(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  /**
   * Maps the property position from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_position(final UserAccount in, final MappingContext context) {
    return in.getPosition();
  }
  
  /**
   * Maps the property position from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_position(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getPosition();
  }
  
  /**
   * Maps the property defaultPerspective from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_defaultPerspective(final UserAccount in, final MappingContext context) {
    return in.getDefaultPerspective();
  }
  
  /**
   * Maps the property defaultPerspective from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_defaultPerspective(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getDefaultPerspective();
  }
  
  /**
   * Maps the property notRegistered from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_notRegistered(final UserAccount in, final MappingContext context) {
    return in.getNotRegistered();
  }
  
  /**
   * Maps the property notRegistered from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_notRegistered(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getNotRegistered();
  }
  
  /**
   * Maps the property enabled from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_enabled(final UserAccount in, final MappingContext context) {
    return in.getEnabled();
  }
  
  /**
   * Maps the property enabled from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_enabled(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getEnabled();
  }
  
  /**
   * Maps the property locked from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_locked(final UserAccount in, final MappingContext context) {
    return in.getLocked();
  }
  
  /**
   * Maps the property locked from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_locked(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getLocked();
  }
  
  /**
   * Maps the property passwordReset from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_passwordReset(final UserAccount in, final MappingContext context) {
    return in.getPasswordReset();
  }
  
  /**
   * Maps the property passwordReset from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_passwordReset(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getPasswordReset();
  }
  
  /**
   * Maps the property superuser from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_superuser(final UserAccount in, final MappingContext context) {
    return in.getSuperuser();
  }
  
  /**
   * Maps the property superuser from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_superuser(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getSuperuser();
  }
  
  /**
   * Maps the property forcePwdChange from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_forcePwdChange(final UserAccount in, final MappingContext context) {
    return in.getForcePwdChange();
  }
  
  /**
   * Maps the property forcePwdChange from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_forcePwdChange(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getForcePwdChange();
  }
  
  /**
   * Maps the property failedAttempt from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_failedAttempt(final UserAccount in, final MappingContext context) {
    return in.getFailedAttempt();
  }
  
  /**
   * Maps the property failedAttempt from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_failedAttempt(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getFailedAttempt();
  }
  
  /**
   * Maps the property successfulAttempt from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_successfulAttempt(final UserAccount in, final MappingContext context) {
    return in.getSuccessfulAttempt();
  }
  
  /**
   * Maps the property successfulAttempt from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_successfulAttempt(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getSuccessfulAttempt();
  }
  
  /**
   * Maps the property cookieHashCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toDto_cookieHashCode(final UserAccount in, final MappingContext context) {
    return in.getCookieHashCode();
  }
  
  /**
   * Maps the property cookieHashCode from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected int toEntity_cookieHashCode(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getCookieHashCode();
  }
  
  /**
   * Maps the property localeTag from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_localeTag(final UserAccount in, final MappingContext context) {
    return in.getLocaleTag();
  }
  
  /**
   * Maps the property localeTag from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_localeTag(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getLocaleTag();
  }
  
  /**
   * Maps the property profileimage from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_profileimage(final UserAccount in, final MappingContext context) {
    return in.getProfileimage();
  }
  
  /**
   * Maps the property profileimage from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_profileimage(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getProfileimage();
  }
  
  /**
   * Maps the property theme from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_theme(final UserAccount in, final MappingContext context) {
    return in.getTheme();
  }
  
  /**
   * Maps the property theme from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_theme(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getTheme();
  }
  
  /**
   * Maps the property printService from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_printService(final UserAccount in, final MappingContext context) {
    return in.getPrintService();
  }
  
  /**
   * Maps the property printService from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_printService(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getPrintService();
  }
  
  /**
   * Maps the property savedProperties from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toDto_savedProperties(final UserAccount in, final MappingContext context) {
    return in.getSavedProperties();
  }
  
  /**
   * Maps the property savedProperties from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toEntity_savedProperties(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getSavedProperties();
  }
  
  /**
   * Maps the property dashBoard from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toDto_dashBoard(final UserAccount in, final MappingContext context) {
    return in.getDashBoard();
  }
  
  /**
   * Maps the property dashBoard from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toEntity_dashBoard(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getDashBoard();
  }
  
  /**
   * Maps the property favorites from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toDto_favorites(final UserAccount in, final MappingContext context) {
    return in.getFavorites();
  }
  
  /**
   * Maps the property favorites from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toEntity_favorites(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getFavorites();
  }
  
  /**
   * Maps the property filters from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toDto_filters(final UserAccount in, final MappingContext context) {
    return in.getFilters();
  }
  
  /**
   * Maps the property filters from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected byte[] toEntity_filters(final UserAccountDto in, final UserAccount parentEntity, final MappingContext context) {
    return in.getFilters();
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(UserAccountDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(UserAccount.class, in);
  }
}
