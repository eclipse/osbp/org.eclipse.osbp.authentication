package org.eclipse.osbp.authentication.account.dtos.service;

import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.account.entities.UserAccount;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;

@SuppressWarnings("all")
public class UserAccountDtoService extends AbstractDTOService<UserAccountDto, UserAccount> {
  public UserAccountDtoService() {
    // set the default persistence ID
    setPersistenceId("authentication");
  }
  
  public Class<UserAccountDto> getDtoClass() {
    return UserAccountDto.class;
  }
  
  public Class<UserAccount> getEntityClass() {
    return UserAccount.class;
  }
  
  public Object getId(final UserAccountDto dto) {
    return dto.getId();
  }
}
