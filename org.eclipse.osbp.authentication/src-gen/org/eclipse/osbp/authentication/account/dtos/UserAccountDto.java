package org.eclipse.osbp.authentication.account.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Hidden;
import org.eclipse.osbp.runtime.common.annotations.Id;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.eclipse.osbp.runtime.common.annotations.UIGroup;
import org.eclipse.osbp.runtime.common.annotations.UniqueEntry;
import org.eclipse.osbp.runtime.common.validation.ErrorSeverity;
import org.eclipse.osbp.runtime.common.validation.InfoSeverity;

@SuppressWarnings("all")
public class UserAccountDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Id
  private String id = java.util.UUID.randomUUID().toString();
  
  @UniqueEntry
  @UIGroup(name = "personal")
  @NotNull(payload = ErrorSeverity.class)
  private String email;
  
  @UniqueEntry
  @UIGroup(name = "personal")
  @NotNull(payload = ErrorSeverity.class)
  private String userName;
  
  @Hidden
  private String password;
  
  @UIGroup(name = "personal")
  @Pattern(regexp = "[0-9]*", payload = InfoSeverity.class)
  private String extraPassword;
  
  @DomainReference
  @FilterDepth(depth = 0)
  @UIGroup(name = "setting")
  private UserGroupDto userGroup;
  
  @Properties(properties = @Property(key = "organization", value = ""))
  @UIGroup(name = "setting")
  private String position;
  
  @Properties(properties = @Property(key = "perspective", value = ""))
  @UIGroup(name = "setting")
  private String defaultPerspective;
  
  @UIGroup(name = "action")
  private boolean notRegistered;
  
  @UIGroup(name = "action")
  private boolean enabled;
  
  @UIGroup(name = "action")
  private boolean locked;
  
  @UIGroup(name = "action")
  private boolean passwordReset;
  
  @UIGroup(name = "personal")
  private boolean superuser;
  
  @UIGroup(name = "action")
  private boolean forcePwdChange;
  
  @UIGroup(name = "statistics")
  private int failedAttempt;
  
  @UIGroup(name = "statistics")
  private int successfulAttempt;
  
  @Hidden
  private int cookieHashCode;
  
  @Properties(properties = @Property(key = "i18n", value = ""))
  @UIGroup(name = "setting")
  private String localeTag;
  
  @Properties(properties = @Property(key = "Blob", value = "2"))
  @UIGroup(name = "personal")
  private String profileimage;
  
  @Properties(properties = @Property(key = "theme", value = ""))
  @UIGroup(name = "setting")
  private String theme;
  
  @Properties(properties = @Property(key = "printservice", value = ""))
  @UIGroup(name = "setting")
  private String printService;
  
  @Hidden
  @Valid
  private byte[] savedProperties;
  
  @Hidden
  @Valid
  private byte[] dashBoard;
  
  @Hidden
  @Valid
  private byte[] favorites;
  
  @Hidden
  @Valid
  private byte[] filters;
  
  public UserAccountDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link UserAccount} to the dto {@link UserAccountDto}.
   * 
   */
  protected void installLazyCollections() {
    
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    return this.id;
  }
  
  /**
   * Sets the <code>id</code> property to this instance.
   * 
   * @param id - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setId(final String id) {
    firePropertyChange("id", this.id, this.id = id );
    				installLazyCollections();
  }
  
  /**
   * Returns the email property or <code>null</code> if not present.
   */
  public String getEmail() {
    return this.email;
  }
  
  /**
   * Sets the <code>email</code> property to this instance.
   * 
   * @param email - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setEmail(final String email) {
    firePropertyChange("email", this.email, this.email = email );
  }
  
  /**
   * Returns the userName property or <code>null</code> if not present.
   */
  public String getUserName() {
    return this.userName;
  }
  
  /**
   * Sets the <code>userName</code> property to this instance.
   * 
   * @param userName - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUserName(final String userName) {
    firePropertyChange("userName", this.userName, this.userName = userName );
  }
  
  /**
   * Returns the password property or <code>null</code> if not present.
   */
  public String getPassword() {
    return this.password;
  }
  
  /**
   * Sets the <code>password</code> property to this instance.
   * 
   * @param password - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPassword(final String password) {
    firePropertyChange("password", this.password, this.password = password );
  }
  
  /**
   * Returns the extraPassword property or <code>null</code> if not present.
   */
  public String getExtraPassword() {
    return this.extraPassword;
  }
  
  /**
   * Sets the <code>extraPassword</code> property to this instance.
   * 
   * @param extraPassword - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setExtraPassword(final String extraPassword) {
    firePropertyChange("extraPassword", this.extraPassword, this.extraPassword = extraPassword );
  }
  
  /**
   * Returns the userGroup property or <code>null</code> if not present.
   */
  public UserGroupDto getUserGroup() {
    return this.userGroup;
  }
  
  /**
   * Sets the <code>userGroup</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>UserGroupDto#
   * userAccount</code> of the <code>userGroup</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link UserGroupDto#setUserAccount(UserGroupDto)
   * 
   * @param userGroup - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUserGroup(final UserGroupDto userGroup) {
    checkDisposed();
    if (this.userGroup != null) {
    	this.userGroup.internalRemoveFromUserAccount(this);
    }
    
    internalSetUserGroup(userGroup);
    
    if (this.userGroup != null) {
    	this.userGroup.internalAddToUserAccount(this);
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalSetUserGroup(final UserGroupDto userGroup) {
    firePropertyChange("userGroup", this.userGroup, this.userGroup = userGroup);
  }
  
  /**
   * Returns the position property or <code>null</code> if not present.
   */
  public String getPosition() {
    return this.position;
  }
  
  /**
   * Sets the <code>position</code> property to this instance.
   * 
   * @param position - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPosition(final String position) {
    firePropertyChange("position", this.position, this.position = position );
  }
  
  /**
   * Returns the defaultPerspective property or <code>null</code> if not present.
   */
  public String getDefaultPerspective() {
    return this.defaultPerspective;
  }
  
  /**
   * Sets the <code>defaultPerspective</code> property to this instance.
   * 
   * @param defaultPerspective - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDefaultPerspective(final String defaultPerspective) {
    firePropertyChange("defaultPerspective", this.defaultPerspective, this.defaultPerspective = defaultPerspective );
  }
  
  /**
   * Returns the notRegistered property or <code>null</code> if not present.
   */
  public boolean getNotRegistered() {
    return this.notRegistered;
  }
  
  /**
   * Sets the <code>notRegistered</code> property to this instance.
   * 
   * @param notRegistered - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setNotRegistered(final boolean notRegistered) {
    firePropertyChange("notRegistered", this.notRegistered, this.notRegistered = notRegistered );
  }
  
  /**
   * Returns the enabled property or <code>null</code> if not present.
   */
  public boolean getEnabled() {
    return this.enabled;
  }
  
  /**
   * Sets the <code>enabled</code> property to this instance.
   * 
   * @param enabled - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setEnabled(final boolean enabled) {
    firePropertyChange("enabled", this.enabled, this.enabled = enabled );
  }
  
  /**
   * Returns the locked property or <code>null</code> if not present.
   */
  public boolean getLocked() {
    return this.locked;
  }
  
  /**
   * Sets the <code>locked</code> property to this instance.
   * 
   * @param locked - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setLocked(final boolean locked) {
    firePropertyChange("locked", this.locked, this.locked = locked );
  }
  
  /**
   * Returns the passwordReset property or <code>null</code> if not present.
   */
  public boolean getPasswordReset() {
    return this.passwordReset;
  }
  
  /**
   * Sets the <code>passwordReset</code> property to this instance.
   * 
   * @param passwordReset - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPasswordReset(final boolean passwordReset) {
    firePropertyChange("passwordReset", this.passwordReset, this.passwordReset = passwordReset );
  }
  
  /**
   * Returns the superuser property or <code>null</code> if not present.
   */
  public boolean getSuperuser() {
    return this.superuser;
  }
  
  /**
   * Sets the <code>superuser</code> property to this instance.
   * 
   * @param superuser - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSuperuser(final boolean superuser) {
    firePropertyChange("superuser", this.superuser, this.superuser = superuser );
  }
  
  /**
   * Returns the forcePwdChange property or <code>null</code> if not present.
   */
  public boolean getForcePwdChange() {
    return this.forcePwdChange;
  }
  
  /**
   * Sets the <code>forcePwdChange</code> property to this instance.
   * 
   * @param forcePwdChange - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setForcePwdChange(final boolean forcePwdChange) {
    firePropertyChange("forcePwdChange", this.forcePwdChange, this.forcePwdChange = forcePwdChange );
  }
  
  /**
   * Returns the failedAttempt property or <code>null</code> if not present.
   */
  public int getFailedAttempt() {
    return this.failedAttempt;
  }
  
  /**
   * Sets the <code>failedAttempt</code> property to this instance.
   * 
   * @param failedAttempt - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFailedAttempt(final int failedAttempt) {
    firePropertyChange("failedAttempt", this.failedAttempt, this.failedAttempt = failedAttempt );
  }
  
  /**
   * Returns the successfulAttempt property or <code>null</code> if not present.
   */
  public int getSuccessfulAttempt() {
    return this.successfulAttempt;
  }
  
  /**
   * Sets the <code>successfulAttempt</code> property to this instance.
   * 
   * @param successfulAttempt - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSuccessfulAttempt(final int successfulAttempt) {
    firePropertyChange("successfulAttempt", this.successfulAttempt, this.successfulAttempt = successfulAttempt );
  }
  
  /**
   * Returns the cookieHashCode property or <code>null</code> if not present.
   */
  public int getCookieHashCode() {
    return this.cookieHashCode;
  }
  
  /**
   * Sets the <code>cookieHashCode</code> property to this instance.
   * 
   * @param cookieHashCode - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setCookieHashCode(final int cookieHashCode) {
    firePropertyChange("cookieHashCode", this.cookieHashCode, this.cookieHashCode = cookieHashCode );
  }
  
  /**
   * Returns the localeTag property or <code>null</code> if not present.
   */
  public String getLocaleTag() {
    return this.localeTag;
  }
  
  /**
   * Sets the <code>localeTag</code> property to this instance.
   * 
   * @param localeTag - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setLocaleTag(final String localeTag) {
    firePropertyChange("localeTag", this.localeTag, this.localeTag = localeTag );
  }
  
  /**
   * Returns the profileimage property or <code>null</code> if not present.
   */
  public String getProfileimage() {
    return this.profileimage;
  }
  
  /**
   * Sets the <code>profileimage</code> property to this instance.
   * 
   * @param profileimage - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setProfileimage(final String profileimage) {
    firePropertyChange("profileimage", this.profileimage, this.profileimage = profileimage );
  }
  
  /**
   * Returns the theme property or <code>null</code> if not present.
   */
  public String getTheme() {
    return this.theme;
  }
  
  /**
   * Sets the <code>theme</code> property to this instance.
   * 
   * @param theme - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setTheme(final String theme) {
    firePropertyChange("theme", this.theme, this.theme = theme );
  }
  
  /**
   * Returns the printService property or <code>null</code> if not present.
   */
  public String getPrintService() {
    return this.printService;
  }
  
  /**
   * Sets the <code>printService</code> property to this instance.
   * 
   * @param printService - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPrintService(final String printService) {
    firePropertyChange("printService", this.printService, this.printService = printService );
  }
  
  /**
   * Returns the savedProperties property or <code>null</code> if not present.
   */
  public byte[] getSavedProperties() {
    return this.savedProperties;
  }
  
  /**
   * Sets the <code>savedProperties</code> property to this instance.
   * 
   * @param savedProperties - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setSavedProperties(final byte[] savedProperties) {
    firePropertyChange("savedProperties", this.savedProperties, this.savedProperties = savedProperties );
  }
  
  /**
   * Returns the dashBoard property or <code>null</code> if not present.
   */
  public byte[] getDashBoard() {
    return this.dashBoard;
  }
  
  /**
   * Sets the <code>dashBoard</code> property to this instance.
   * 
   * @param dashBoard - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDashBoard(final byte[] dashBoard) {
    firePropertyChange("dashBoard", this.dashBoard, this.dashBoard = dashBoard );
  }
  
  /**
   * Returns the favorites property or <code>null</code> if not present.
   */
  public byte[] getFavorites() {
    return this.favorites;
  }
  
  /**
   * Sets the <code>favorites</code> property to this instance.
   * 
   * @param favorites - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFavorites(final byte[] favorites) {
    firePropertyChange("favorites", this.favorites, this.favorites = favorites );
  }
  
  /**
   * Returns the filters property or <code>null</code> if not present.
   */
  public byte[] getFilters() {
    return this.filters;
  }
  
  /**
   * Sets the <code>filters</code> property to this instance.
   * 
   * @param filters - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setFilters(final byte[] filters) {
    firePropertyChange("filters", this.filters, this.filters = filters );
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserAccountDto other = (UserAccountDto) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	// no super class available to forward event
    }
  }
}
