package org.eclipse.osbp.authentication.account.dtos.mapper;

import org.eclipse.osbp.authentication.account.dtos.FilterDto;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.account.entities.Filter;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link FilterDto} to and from the entity {@link Filter}.
 * 
 */
@SuppressWarnings("all")
public class FilterDtoMapper<DTO extends FilterDto, ENTITY extends Filter> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public Filter createEntity() {
    return new Filter();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public FilterDto createDto() {
    return new FilterDto();
  }
  
  /**
   * Maps the entity {@link Filter} to the dto {@link FilterDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final FilterDto dto, final Filter entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setFilter(toDto_filter(entity, context));
    dto.setInvers(toDto_invers(entity, context));
    dto.setUserGroup(toDto_userGroup(entity, context));
  }
  
  /**
   * Maps the dto {@link FilterDto} to the entity {@link Filter}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final FilterDto dto, final Filter entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setFilter(toEntity_filter(dto, entity, context));
    entity.setInvers(toEntity_invers(dto, entity, context));
    entity.setUserGroup(toEntity_userGroup(dto, entity, context));
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_id(final Filter in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_id(final FilterDto in, final Filter parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property filter from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_filter(final Filter in, final MappingContext context) {
    return in.getFilter();
  }
  
  /**
   * Maps the property filter from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_filter(final FilterDto in, final Filter parentEntity, final MappingContext context) {
    return in.getFilter();
  }
  
  /**
   * Maps the property invers from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toDto_invers(final Filter in, final MappingContext context) {
    return in.getInvers();
  }
  
  /**
   * Maps the property invers from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected boolean toEntity_invers(final FilterDto in, final Filter parentEntity, final MappingContext context) {
    return in.getInvers();
  }
  
  /**
   * Maps the property userGroup from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped dto
   * 
   */
  protected UserGroupDto toDto_userGroup(final Filter in, final MappingContext context) {
    if(in.getUserGroup() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup>) getToDtoMapper(UserGroupDto.class, in.getUserGroup().getClass());
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    	UserGroupDto dto = null;
    	dto = context.get(mapper.createDtoHash(in.getUserGroup()));
    	if(dto != null) {
    		if(context.isRefresh()){
    			mapper.mapToDTO(dto, in.getUserGroup(), context);
    		}
    		return dto;
    	}
    	
    	context.increaseLevel();
    	dto = mapper.createDto();
    	mapper.mapToDTO(dto, in.getUserGroup(), context);
    	context.decreaseLevel();
    	return dto;
    } else {
    	return null;
    }
  }
  
  /**
   * Maps the property userGroup from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return the mapped entity
   * 
   */
  protected UserGroup toEntity_userGroup(final FilterDto in, final Filter parentEntity, final MappingContext context) {
    if(in.getUserGroup() != null) {
    	// find a mapper that knows how to map the concrete input type.
    	org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup> mapper = (org.eclipse.osbp.dsl.dto.lib.IMapper<UserGroupDto, UserGroup>) getToEntityMapper(in.getUserGroup().getClass(), UserGroup.class);
    	if(mapper == null) {
    		throw new IllegalStateException("Mapper must not be null!");
    	}
    
    	UserGroup entity = null;
    	entity = context.get(mapper.createEntityHash(in.getUserGroup()));
    	if(entity != null) {
    		return entity;
    	} else {
    		entity = (UserGroup) context
    			.findEntityByEntityManager(UserGroup.class, in.getUserGroup().getId());
    		if (entity != null) {
    			context.register(mapper.createEntityHash(in.getUserGroup()), entity);
    			return entity;
    		}
    	}
    
    	entity = mapper.createEntity();
    	mapper.mapToEntity(in.getUserGroup(), entity, context);	
    	return entity;
    } else {
    	return null;
    }	
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(FilterDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(Filter.class, in);
  }
}
