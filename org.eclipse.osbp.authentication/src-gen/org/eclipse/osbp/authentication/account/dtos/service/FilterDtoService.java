package org.eclipse.osbp.authentication.account.dtos.service;

import org.eclipse.osbp.authentication.account.dtos.FilterDto;
import org.eclipse.osbp.authentication.account.entities.Filter;
import org.eclipse.osbp.dsl.dto.lib.services.impl.AbstractDTOService;

@SuppressWarnings("all")
public class FilterDtoService extends AbstractDTOService<FilterDto, Filter> {
  public FilterDtoService() {
    // set the default persistence ID
    setPersistenceId("authentication");
  }
  
  public Class<FilterDto> getDtoClass() {
    return FilterDto.class;
  }
  
  public Class<Filter> getEntityClass() {
    return Filter.class;
  }
  
  public Object getId(final FilterDto dto) {
    return dto.getId();
  }
}
