package org.eclipse.osbp.authentication.account.dtos;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.eclipse.osbp.authentication.account.dtos.FilterDto;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.dsl.common.datatypes.IDto;
import org.eclipse.osbp.runtime.common.annotations.AsTable;
import org.eclipse.osbp.runtime.common.annotations.Dirty;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.eclipse.osbp.runtime.common.annotations.DomainReference;
import org.eclipse.osbp.runtime.common.annotations.FilterDepth;
import org.eclipse.osbp.runtime.common.annotations.Id;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.eclipse.osbp.runtime.common.annotations.UIGroup;
import org.eclipse.osbp.runtime.common.annotations.UniqueEntry;
import org.eclipse.osbp.runtime.common.validation.ErrorSeverity;

@SuppressWarnings("all")
public class UserGroupDto implements IDto, Serializable, PropertyChangeListener {
  private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
  
  @Dispose
  private boolean disposed;
  
  @Dirty
  private transient boolean dirty;
  
  @Id
  private String id = java.util.UUID.randomUUID().toString();
  
  @DomainKey
  @UniqueEntry
  @UIGroup(name = "common")
  @NotNull(payload = ErrorSeverity.class)
  private String userGroupName;
  
  @Properties(properties = @Property(key = "organization", value = ""))
  @UIGroup(name = "organization")
  private String position;
  
  @Properties(properties = @Property(key = "perspective", value = ""))
  @UIGroup(name = "setting")
  private String defaultPerspective;
  
  @Properties(properties = @Property(key = "i18n", value = ""))
  @UIGroup(name = "setting")
  private String localeTag;
  
  @Properties(properties = @Property(key = "theme", value = ""))
  @UIGroup(name = "setting")
  private String theme;
  
  @Properties(properties = @Property(key = "printservice", value = ""))
  @UIGroup(name = "setting")
  private String printService;
  
  @DomainReference
  @FilterDepth(depth = 0)
  @AsTable
  private List<UserAccountDto> userAccount;
  
  @DomainReference
  @FilterDepth(depth = 0)
  private List<FilterDto> userGroupFilter;
  
  public UserGroupDto() {
    installLazyCollections();
  }
  
  /**
   * Installs lazy collection resolving for entity {@link UserGroup} to the dto {@link UserGroupDto}.
   * 
   */
  protected void installLazyCollections() {
    userAccount = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				UserAccountDto.class, "userGroup.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
    userGroupFilter = new org.eclipse.osbp.dsl.dto.lib.OppositeDtoList<>(
    				org.eclipse.osbp.dsl.dto.lib.MappingContext.getCurrent(),
    				FilterDto.class, "userGroup.id",
    				(java.util.function.Supplier<Object> & Serializable) () -> this.getId(), this);
  }
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(PropertyChangeListener)
   */
  public void addPropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#addPropertyChangeListener(String, PropertyChangeListener)
   */
  public void addPropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.addPropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(PropertyChangeListener)
   */
  public void removePropertyChangeListener(final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(listener);
  }
  
  /**
   * @see PropertyChangeSupport#removePropertyChangeListener(String, PropertyChangeListener)
   */
  public void removePropertyChangeListener(final String propertyName, final PropertyChangeListener listener) {
    propertyChangeSupport.removePropertyChangeListener(propertyName, listener);
  }
  
  /**
   * @see PropertyChangeSupport#firePropertyChange(String, Object, Object)
   */
  public void firePropertyChange(final String propertyName, final Object oldValue, final Object newValue) {
    propertyChangeSupport.firePropertyChange(propertyName, oldValue, newValue);
  }
  
  /**
   * @return true, if the object is dirty. 
   * 
   */
  public boolean isDirty() {
    return dirty;
  }
  
  /**
   * Sets the dirty state of this object.
   * 
   */
  public void setDirty(final boolean dirty) {
    firePropertyChange("dirty", this.dirty, this.dirty = dirty );
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br/>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    firePropertyChange("disposed", this.disposed, this.disposed = true);
  }
  
  /**
   * Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    return this.id;
  }
  
  /**
   * Sets the <code>id</code> property to this instance.
   * 
   * @param id - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setId(final String id) {
    firePropertyChange("id", this.id, this.id = id );
    				installLazyCollections();
  }
  
  /**
   * Returns the userGroupName property or <code>null</code> if not present.
   */
  public String getUserGroupName() {
    return this.userGroupName;
  }
  
  /**
   * Sets the <code>userGroupName</code> property to this instance.
   * 
   * @param userGroupName - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUserGroupName(final String userGroupName) {
    firePropertyChange("userGroupName", this.userGroupName, this.userGroupName = userGroupName );
  }
  
  /**
   * Returns the position property or <code>null</code> if not present.
   */
  public String getPosition() {
    return this.position;
  }
  
  /**
   * Sets the <code>position</code> property to this instance.
   * 
   * @param position - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPosition(final String position) {
    firePropertyChange("position", this.position, this.position = position );
  }
  
  /**
   * Returns the defaultPerspective property or <code>null</code> if not present.
   */
  public String getDefaultPerspective() {
    return this.defaultPerspective;
  }
  
  /**
   * Sets the <code>defaultPerspective</code> property to this instance.
   * 
   * @param defaultPerspective - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setDefaultPerspective(final String defaultPerspective) {
    firePropertyChange("defaultPerspective", this.defaultPerspective, this.defaultPerspective = defaultPerspective );
  }
  
  /**
   * Returns the localeTag property or <code>null</code> if not present.
   */
  public String getLocaleTag() {
    return this.localeTag;
  }
  
  /**
   * Sets the <code>localeTag</code> property to this instance.
   * 
   * @param localeTag - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setLocaleTag(final String localeTag) {
    firePropertyChange("localeTag", this.localeTag, this.localeTag = localeTag );
  }
  
  /**
   * Returns the theme property or <code>null</code> if not present.
   */
  public String getTheme() {
    return this.theme;
  }
  
  /**
   * Sets the <code>theme</code> property to this instance.
   * 
   * @param theme - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setTheme(final String theme) {
    firePropertyChange("theme", this.theme, this.theme = theme );
  }
  
  /**
   * Returns the printService property or <code>null</code> if not present.
   */
  public String getPrintService() {
    return this.printService;
  }
  
  /**
   * Sets the <code>printService</code> property to this instance.
   * 
   * @param printService - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setPrintService(final String printService) {
    firePropertyChange("printService", this.printService, this.printService = printService );
  }
  
  /**
   * Returns an unmodifiable list of userAccount.
   */
  public List<UserAccountDto> getUserAccount() {
    return Collections.unmodifiableList(internalGetUserAccount());
  }
  
  /**
   * Returns the list of <code>UserAccountDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<UserAccountDto> internalGetUserAccount() {
    if (this.userAccount == null) {
      this.userAccount = new java.util.ArrayList<UserAccountDto>();
    }
    return this.userAccount;
  }
  
  /**
   * Adds the given userAccountDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>UserAccountDto#userGroup</code> of the <code>userAccountDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link UserAccountDto#setUserGroup(UserAccountDto)}.
   * 
   * @param userAccountDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToUserAccount(final UserAccountDto userAccountDto) {
    checkDisposed();
    
    userAccountDto.setUserGroup(this);
  }
  
  /**
   * Removes the given userAccountDto from this object. <p>
   * 
   * @param userAccountDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromUserAccount(final UserAccountDto userAccountDto) {
    checkDisposed();
    
    userAccountDto.setUserGroup(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToUserAccount(final UserAccountDto userAccountDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<UserAccountDto> oldList = null;
    		if(internalGetUserAccount() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetUserAccount()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetUserAccount());
    		}
    		internalGetUserAccount().add(userAccountDto);
    		firePropertyChange("userAccount", oldList, internalGetUserAccount());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromUserAccount(final UserAccountDto userAccountDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<UserAccountDto> oldList = null;
    	if(internalGetUserAccount() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetUserAccount()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetUserAccount());
    	}
    	internalGetUserAccount().remove(userAccountDto);
    	firePropertyChange("userAccount", oldList, internalGetUserAccount());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetUserAccount().remove(userAccountDto);
    }
  }
  
  /**
   * Sets the <code>userAccount</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>UserAccountDto#
   * userGroup</code> of the <code>userAccount</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link UserAccountDto#setUserGroup(UserAccountDto)
   * 
   * @param userAccount - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUserAccount(final List<UserAccountDto> userAccount) {
    checkDisposed();
    for (UserAccountDto dto : internalGetUserAccount().toArray(new UserAccountDto[this.userAccount.size()])) {
    	removeFromUserAccount(dto);
    }
    
    if(userAccount == null) {
    	return;
    }
    
    for (UserAccountDto dto : userAccount) {
    	addToUserAccount(dto);
    }
  }
  
  /**
   * Returns an unmodifiable list of userGroupFilter.
   */
  public List<FilterDto> getUserGroupFilter() {
    return Collections.unmodifiableList(internalGetUserGroupFilter());
  }
  
  /**
   * Returns the list of <code>FilterDto</code>s thereby lazy initializing it. For internal use only!
   * 
   * @return list - the resulting list
   * 
   */
  public List<FilterDto> internalGetUserGroupFilter() {
    if (this.userGroupFilter == null) {
      this.userGroupFilter = new java.util.ArrayList<FilterDto>();
    }
    return this.userGroupFilter;
  }
  
  /**
   * Adds the given filterDto to this object. <p>
   * Since the reference is a composition reference, the opposite reference <code>FilterDto#userGroup</code> of the <code>filterDto</code> will be handled automatically and no further coding is required to keep them in sync.<p>
   * See {@link FilterDto#setUserGroup(FilterDto)}.
   * 
   * @param filterDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void addToUserGroupFilter(final FilterDto filterDto) {
    checkDisposed();
    
    filterDto.setUserGroup(this);
  }
  
  /**
   * Removes the given filterDto from this object. <p>
   * 
   * @param filterDto - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void removeFromUserGroupFilter(final FilterDto filterDto) {
    checkDisposed();
    
    filterDto.setUserGroup(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToUserGroupFilter(final FilterDto filterDto) {
    
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    		List<FilterDto> oldList = null;
    		if(internalGetUserGroupFilter() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    			oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetUserGroupFilter()).copy();
    		} else {
    			oldList = new java.util.ArrayList<>(internalGetUserGroupFilter());
    		}
    		internalGetUserGroupFilter().add(filterDto);
    		firePropertyChange("userGroupFilter", oldList, internalGetUserGroupFilter());
    }
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromUserGroupFilter(final FilterDto filterDto) {
    if(!org.eclipse.osbp.dsl.dto.lib.MappingContext.isMappingMode()) {
    	List<FilterDto> oldList = null;
    	if(internalGetUserGroupFilter() instanceof org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) {
    		oldList = ((org.eclipse.osbp.dsl.dto.lib.AbstractOppositeDtoList) internalGetUserGroupFilter()).copy();
    	} else {
    		oldList = new java.util.ArrayList<>(internalGetUserGroupFilter());
    	}
    	internalGetUserGroupFilter().remove(filterDto);
    	firePropertyChange("userGroupFilter", oldList, internalGetUserGroupFilter());	
    }else{
    	// in mapping mode, we do NOT resolve any collection
    	internalGetUserGroupFilter().remove(filterDto);
    }
  }
  
  /**
   * Sets the <code>userGroupFilter</code> property to this instance.
   * Since the reference has an opposite reference, the opposite <code>FilterDto#
   * userGroup</code> of the <code>userGroupFilter</code> will be handled automatically and no 
   * further coding is required to keep them in sync.<p>
   * See {@link FilterDto#setUserGroup(FilterDto)
   * 
   * @param userGroupFilter - the property
   * @throws RuntimeException if instance is <code>disposed</code>
   * 
   */
  public void setUserGroupFilter(final List<FilterDto> userGroupFilter) {
    checkDisposed();
    for (FilterDto dto : internalGetUserGroupFilter().toArray(new FilterDto[this.userGroupFilter.size()])) {
    	removeFromUserGroupFilter(dto);
    }
    
    if(userGroupFilter == null) {
    	return;
    }
    
    for (FilterDto dto : userGroupFilter) {
    	addToUserGroupFilter(dto);
    }
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserGroupDto other = (UserGroupDto) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  public void propertyChange(final java.beans.PropertyChangeEvent event) {
    Object source = event.getSource();
    
    // forward the event from embeddable beans to all listeners. So the parent of the embeddable
    // bean will become notified and its dirty state can be handled properly
    { 
    	// no super class available to forward event
    }
  }
}
