package org.eclipse.osbp.authentication.account.dtos.mapper;

import java.util.List;
import org.eclipse.osbp.authentication.account.dtos.FilterDto;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.account.entities.Filter;
import org.eclipse.osbp.authentication.account.entities.UserAccount;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.dto.lib.IMapper;
import org.eclipse.osbp.dsl.dto.lib.IMapperAccess;
import org.eclipse.osbp.dsl.dto.lib.MappingContext;

/**
 * This class maps the dto {@link UserGroupDto} to and from the entity {@link UserGroup}.
 * 
 */
@SuppressWarnings("all")
public class UserGroupDtoMapper<DTO extends UserGroupDto, ENTITY extends UserGroup> implements IMapper<DTO, ENTITY> {
  private IMapperAccess mapperAccess;
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToDtoMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToDtoMapper(dtoClass, entityClass);
  }
  
  /**
   * Returns the mapper instance that may map between the given dto and entity. Or <code>null</code> if no mapper is available.
   * 
   * @param dtoClass - the class of the dto that should be mapped
   * @param entityClass - the class of the entity that should be mapped
   * @return the mapper instance or <code>null</code>
   */
  protected <D, E> IMapper<D, E> getToEntityMapper(final Class<D> dtoClass, final Class<E> entityClass) {
    return mapperAccess.getToEntityMapper(dtoClass, entityClass);
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void bindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = mapperAccess;
  }
  
  /**
   * Called by OSGi-DS. Binds the mapper access service.
   * 
   * @param service - The mapper access service
   * 
   */
  protected void unbindMapperAccess(final IMapperAccess mapperAccess) {
    this.mapperAccess = null;
  }
  
  /**
   * Creates a new instance of the entity
   */
  public UserGroup createEntity() {
    return new UserGroup();
  }
  
  /**
   * Creates a new instance of the dto
   */
  public UserGroupDto createDto() {
    return new UserGroupDto();
  }
  
  /**
   * Maps the entity {@link UserGroup} to the dto {@link UserGroupDto}.
   * 
   * @param dto - The target dto
   * @param entity - The source entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToDTO(final UserGroupDto dto, final UserGroup entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    context.register(createDtoHash(entity), dto);
    
    dto.setId(toDto_id(entity, context));
    dto.setUserGroupName(toDto_userGroupName(entity, context));
    dto.setPosition(toDto_position(entity, context));
    dto.setDefaultPerspective(toDto_defaultPerspective(entity, context));
    dto.setLocaleTag(toDto_localeTag(entity, context));
    dto.setTheme(toDto_theme(entity, context));
    dto.setPrintService(toDto_printService(entity, context));
  }
  
  /**
   * Maps the dto {@link UserGroupDto} to the entity {@link UserGroup}.
   * 
   * @param dto - The source dto
   * @param entity - The target entity
   * @param context - The context to get information about depth,...
   * 
   */
  public void mapToEntity(final UserGroupDto dto, final UserGroup entity, final MappingContext context) {
    if(context == null){
    	throw new IllegalArgumentException("Please pass a context!");
    }
    
    context.register(createEntityHash(dto), entity);
    context.registerMappingRoot(createEntityHash(dto), dto);
    
    entity.setId(toEntity_id(dto, entity, context));
    entity.setUserGroupName(toEntity_userGroupName(dto, entity, context));
    entity.setPosition(toEntity_position(dto, entity, context));
    entity.setDefaultPerspective(toEntity_defaultPerspective(dto, entity, context));
    entity.setLocaleTag(toEntity_localeTag(dto, entity, context));
    entity.setTheme(toEntity_theme(dto, entity, context));
    entity.setPrintService(toEntity_printService(dto, entity, context));
    toEntity_userAccount(dto, entity, context);
    toEntity_userGroupFilter(dto, entity, context);
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_id(final UserGroup in, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property id from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_id(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getId();
  }
  
  /**
   * Maps the property userGroupName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_userGroupName(final UserGroup in, final MappingContext context) {
    return in.getUserGroupName();
  }
  
  /**
   * Maps the property userGroupName from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_userGroupName(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getUserGroupName();
  }
  
  /**
   * Maps the property position from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_position(final UserGroup in, final MappingContext context) {
    return in.getPosition();
  }
  
  /**
   * Maps the property position from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_position(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getPosition();
  }
  
  /**
   * Maps the property defaultPerspective from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_defaultPerspective(final UserGroup in, final MappingContext context) {
    return in.getDefaultPerspective();
  }
  
  /**
   * Maps the property defaultPerspective from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_defaultPerspective(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getDefaultPerspective();
  }
  
  /**
   * Maps the property localeTag from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_localeTag(final UserGroup in, final MappingContext context) {
    return in.getLocaleTag();
  }
  
  /**
   * Maps the property localeTag from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_localeTag(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getLocaleTag();
  }
  
  /**
   * Maps the property theme from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_theme(final UserGroup in, final MappingContext context) {
    return in.getTheme();
  }
  
  /**
   * Maps the property theme from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_theme(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getTheme();
  }
  
  /**
   * Maps the property printService from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toDto_printService(final UserGroup in, final MappingContext context) {
    return in.getPrintService();
  }
  
  /**
   * Maps the property printService from the given entity to dto property.
   * 
   * @param in - The source entity
   * @param parentEntity - The parentEntity
   * @param context - The context to get information about depth,...
   * @return the mapped value
   * 
   */
  protected String toEntity_printService(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    return in.getPrintService();
  }
  
  /**
   * Maps the property userAccount from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<UserAccountDto> toDto_userAccount(final UserGroup in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property userAccount from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<UserAccount> toEntity_userAccount(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<UserAccountDto, UserAccount> mapper = getToEntityMapper(UserAccountDto.class, UserAccount.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<UserAccountDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<UserAccountDto>) in.internalGetUserAccount();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToUserAccount,
    		parentEntity::internalRemoveFromUserAccount);
    return null;
  }
  
  /**
   * Maps the property userGroupFilter from the given entity to the dto.
   * 
   * @param in - The source entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped dtos
   * 
   */
  protected List<FilterDto> toDto_userGroupFilter(final UserGroup in, final MappingContext context) {
    // nothing to do here. Mapping is done by OppositeLists
    return null;
  }
  
  /**
   * Maps the property userGroupFilter from the given dto to the entity.
   * 
   * @param in - The source dto
   * @param parentEntity - The parent entity
   * @param context - The context to get information about depth,...
   * @return A list of mapped entities
   * 
   */
  protected List<Filter> toEntity_userGroupFilter(final UserGroupDto in, final UserGroup parentEntity, final MappingContext context) {
    org.eclipse.osbp.dsl.dto.lib.IMapper<FilterDto, Filter> mapper = getToEntityMapper(FilterDto.class, Filter.class);
    if(mapper == null) {
    	throw new IllegalStateException("Mapper must not be null!");
    }
    
    org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<FilterDto> childsList = 
    	(org.eclipse.osbp.dsl.dto.lib.IEntityMappingList<FilterDto>) in.internalGetUserGroupFilter();
    
    // if entities are being added, then they are passed to
    // #addToContainerChilds of the parent entity. So the container ref is setup
    // properly!
    // if entities are being removed, then they are passed to the
    // #internalRemoveFromChilds method of the parent entity. So they are
    // removed directly from the list of entities.
    if ( childsList != null ) childsList.mapToEntity(mapper,
    		parentEntity::addToUserGroupFilter,
    		parentEntity::internalRemoveFromUserGroupFilter);
    return null;
  }
  
  public String createDtoHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(UserGroupDto.class, in);
  }
  
  public String createEntityHash(final Object in) {
    return org.eclipse.osbp.runtime.common.hash.HashUtil.createObjectWithIdHash(UserGroup.class, in);
  }
}
