package org.eclipse.osbp.authentication.account.entities;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.Hidden;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.eclipse.osbp.runtime.common.annotations.UIGroup;
import org.eclipse.osbp.runtime.common.annotations.UniqueEntry;
import org.eclipse.osbp.runtime.common.validation.ErrorSeverity;
import org.eclipse.osbp.runtime.common.validation.InfoSeverity;

@Cacheable(false)
@Entity
@Table(name = "USER_ACCOUNT", indexes = @Index(name = "USER_ACCOUNTINDEX_USER_NAME", unique = true, columnList = "USER_NAME"))
@DiscriminatorValue(value = "USER_ACCOUNT")
@SuppressWarnings("all")
public class UserAccount implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "ID")
  private String id = java.util.UUID.randomUUID().toString();
  
  @Column(name = "EMAIL")
  @UniqueEntry
  @UIGroup(name = "personal")
  @NotNull(payload = ErrorSeverity.class)
  private String email;
  
  @Column(name = "USER_NAME")
  @UniqueEntry
  @UIGroup(name = "personal")
  @NotNull(payload = ErrorSeverity.class)
  private String userName;
  
  @Hidden
  @Column(name = "PASSWORD")
  private String password;
  
  @Column(name = "EXTRA_PASSWORD")
  @UIGroup(name = "personal")
  @Pattern(regexp = "[0-9]*", payload = InfoSeverity.class)
  private String extraPassword;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "USER_GROUP_ID")
  private UserGroup userGroup;
  
  @Column(name = "POSITION")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "organization", value = ""))
  private String position;
  
  @Column(name = "DEFAULT_PERSPECTIVE")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "perspective", value = ""))
  private String defaultPerspective;
  
  @Column(name = "NOT_REGISTERED")
  @UIGroup(name = "action")
  private boolean notRegistered;
  
  @Column(name = "ENABLED")
  @UIGroup(name = "action")
  private boolean enabled;
  
  @Column(name = "LOCKED")
  @UIGroup(name = "action")
  private boolean locked;
  
  @Column(name = "PASSWORD_RESET")
  @UIGroup(name = "action")
  private boolean passwordReset;
  
  @Column(name = "SUPERUSER")
  @UIGroup(name = "personal")
  private boolean superuser;
  
  @Column(name = "FORCE_PWD_CHANGE")
  @UIGroup(name = "action")
  private boolean forcePwdChange;
  
  @Column(name = "FAILED_ATTEMPT")
  @UIGroup(name = "statistics")
  private int failedAttempt;
  
  @Column(name = "SUCCESSFUL_ATTEMPT")
  @UIGroup(name = "statistics")
  private int successfulAttempt;
  
  @Hidden
  @Column(name = "COOKIE_HASH_CODE")
  private int cookieHashCode;
  
  @Column(name = "LOCALE_TAG")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "i18n", value = ""))
  private String localeTag;
  
  @Column(name = "PROFILEIMAGE")
  @UIGroup(name = "personal")
  @Properties(properties = @Property(key = "Blob", value = "2"))
  private String profileimage;
  
  @Column(name = "THEME")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "theme", value = ""))
  private String theme;
  
  @Column(name = "PRINT_SERVICE")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "printservice", value = ""))
  private String printService;
  
  @Hidden
  @Column(name = "SAVED_PROPERTIES")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Valid
  private byte[] savedProperties;
  
  @Hidden
  @Column(name = "DASH_BOARD")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Valid
  private byte[] dashBoard;
  
  @Hidden
  @Column(name = "FAVORITES")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Valid
  private byte[] favorites;
  
  @Hidden
  @Column(name = "FILTERS")
  @Lob
  @Basic(fetch = FetchType.LAZY)
  @Valid
  private byte[] filters;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final String id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the email property or <code>null</code> if not present.
   */
  public String getEmail() {
    checkDisposed();
    return this.email;
  }
  
  /**
   * Sets the email property to this instance.
   */
  public void setEmail(final String email) {
    checkDisposed();
    this.email = email;
  }
  
  /**
   * @return Returns the userName property or <code>null</code> if not present.
   */
  public String getUserName() {
    checkDisposed();
    return this.userName;
  }
  
  /**
   * Sets the userName property to this instance.
   */
  public void setUserName(final String userName) {
    checkDisposed();
    this.userName = userName;
  }
  
  /**
   * @return Returns the password property or <code>null</code> if not present.
   */
  public String getPassword() {
    checkDisposed();
    return this.password;
  }
  
  /**
   * Sets the password property to this instance.
   */
  public void setPassword(final String password) {
    checkDisposed();
    this.password = password;
  }
  
  /**
   * @return Returns the extraPassword property or <code>null</code> if not present.
   */
  public String getExtraPassword() {
    checkDisposed();
    return this.extraPassword;
  }
  
  /**
   * Sets the extraPassword property to this instance.
   */
  public void setExtraPassword(final String extraPassword) {
    checkDisposed();
    this.extraPassword = extraPassword;
  }
  
  /**
   * @return Returns the userGroup property or <code>null</code> if not present.
   */
  public UserGroup getUserGroup() {
    checkDisposed();
    return this.userGroup;
  }
  
  /**
   * Sets the userGroup property to this instance.
   * Since the reference is a container reference, the opposite reference (UserGroup.userAccount)
   * of the userGroup will be handled automatically and no further coding is required to keep them in sync.
   * See {@link UserGroup#setUserAccount(UserGroup)}.
   */
  public void setUserGroup(final UserGroup userGroup) {
    checkDisposed();
    if (this.userGroup != null) {
      this.userGroup.internalRemoveFromUserAccount(this);
    }
    internalSetUserGroup(userGroup);
    if (this.userGroup != null) {
      this.userGroup.internalAddToUserAccount(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetUserGroup(final UserGroup userGroup) {
    this.userGroup = userGroup;
  }
  
  /**
   * @return Returns the position property or <code>null</code> if not present.
   */
  public String getPosition() {
    checkDisposed();
    return this.position;
  }
  
  /**
   * Sets the position property to this instance.
   */
  public void setPosition(final String position) {
    checkDisposed();
    this.position = position;
  }
  
  /**
   * @return Returns the defaultPerspective property or <code>null</code> if not present.
   */
  public String getDefaultPerspective() {
    checkDisposed();
    return this.defaultPerspective;
  }
  
  /**
   * Sets the defaultPerspective property to this instance.
   */
  public void setDefaultPerspective(final String defaultPerspective) {
    checkDisposed();
    this.defaultPerspective = defaultPerspective;
  }
  
  /**
   * @return Returns the notRegistered property or <code>null</code> if not present.
   */
  public boolean getNotRegistered() {
    checkDisposed();
    return this.notRegistered;
  }
  
  /**
   * Sets the notRegistered property to this instance.
   */
  public void setNotRegistered(final boolean notRegistered) {
    checkDisposed();
    this.notRegistered = notRegistered;
  }
  
  /**
   * @return Returns the enabled property or <code>null</code> if not present.
   */
  public boolean getEnabled() {
    checkDisposed();
    return this.enabled;
  }
  
  /**
   * Sets the enabled property to this instance.
   */
  public void setEnabled(final boolean enabled) {
    checkDisposed();
    this.enabled = enabled;
  }
  
  /**
   * @return Returns the locked property or <code>null</code> if not present.
   */
  public boolean getLocked() {
    checkDisposed();
    return this.locked;
  }
  
  /**
   * Sets the locked property to this instance.
   */
  public void setLocked(final boolean locked) {
    checkDisposed();
    this.locked = locked;
  }
  
  /**
   * @return Returns the passwordReset property or <code>null</code> if not present.
   */
  public boolean getPasswordReset() {
    checkDisposed();
    return this.passwordReset;
  }
  
  /**
   * Sets the passwordReset property to this instance.
   */
  public void setPasswordReset(final boolean passwordReset) {
    checkDisposed();
    this.passwordReset = passwordReset;
  }
  
  /**
   * @return Returns the superuser property or <code>null</code> if not present.
   */
  public boolean getSuperuser() {
    checkDisposed();
    return this.superuser;
  }
  
  /**
   * Sets the superuser property to this instance.
   */
  public void setSuperuser(final boolean superuser) {
    checkDisposed();
    this.superuser = superuser;
  }
  
  /**
   * @return Returns the forcePwdChange property or <code>null</code> if not present.
   */
  public boolean getForcePwdChange() {
    checkDisposed();
    return this.forcePwdChange;
  }
  
  /**
   * Sets the forcePwdChange property to this instance.
   */
  public void setForcePwdChange(final boolean forcePwdChange) {
    checkDisposed();
    this.forcePwdChange = forcePwdChange;
  }
  
  /**
   * @return Returns the failedAttempt property or <code>null</code> if not present.
   */
  public int getFailedAttempt() {
    checkDisposed();
    return this.failedAttempt;
  }
  
  /**
   * Sets the failedAttempt property to this instance.
   */
  public void setFailedAttempt(final int failedAttempt) {
    checkDisposed();
    this.failedAttempt = failedAttempt;
  }
  
  /**
   * @return Returns the successfulAttempt property or <code>null</code> if not present.
   */
  public int getSuccessfulAttempt() {
    checkDisposed();
    return this.successfulAttempt;
  }
  
  /**
   * Sets the successfulAttempt property to this instance.
   */
  public void setSuccessfulAttempt(final int successfulAttempt) {
    checkDisposed();
    this.successfulAttempt = successfulAttempt;
  }
  
  /**
   * @return Returns the cookieHashCode property or <code>null</code> if not present.
   */
  public int getCookieHashCode() {
    checkDisposed();
    return this.cookieHashCode;
  }
  
  /**
   * Sets the cookieHashCode property to this instance.
   */
  public void setCookieHashCode(final int cookieHashCode) {
    checkDisposed();
    this.cookieHashCode = cookieHashCode;
  }
  
  /**
   * @return Returns the localeTag property or <code>null</code> if not present.
   */
  public String getLocaleTag() {
    checkDisposed();
    return this.localeTag;
  }
  
  /**
   * Sets the localeTag property to this instance.
   */
  public void setLocaleTag(final String localeTag) {
    checkDisposed();
    this.localeTag = localeTag;
  }
  
  /**
   * @return Returns the profileimage property or <code>null</code> if not present.
   */
  public String getProfileimage() {
    checkDisposed();
    return this.profileimage;
  }
  
  /**
   * Sets the profileimage property to this instance.
   */
  public void setProfileimage(final String profileimage) {
    checkDisposed();
    this.profileimage = profileimage;
  }
  
  /**
   * @return Returns the theme property or <code>null</code> if not present.
   */
  public String getTheme() {
    checkDisposed();
    return this.theme;
  }
  
  /**
   * Sets the theme property to this instance.
   */
  public void setTheme(final String theme) {
    checkDisposed();
    this.theme = theme;
  }
  
  /**
   * @return Returns the printService property or <code>null</code> if not present.
   */
  public String getPrintService() {
    checkDisposed();
    return this.printService;
  }
  
  /**
   * Sets the printService property to this instance.
   */
  public void setPrintService(final String printService) {
    checkDisposed();
    this.printService = printService;
  }
  
  /**
   * @return Returns the savedProperties property or <code>null</code> if not present.
   */
  public byte[] getSavedProperties() {
    checkDisposed();
    return this.savedProperties;
  }
  
  /**
   * Sets the savedProperties property to this instance.
   */
  public void setSavedProperties(final byte[] savedProperties) {
    checkDisposed();
    this.savedProperties = savedProperties;
  }
  
  /**
   * @return Returns the dashBoard property or <code>null</code> if not present.
   */
  public byte[] getDashBoard() {
    checkDisposed();
    return this.dashBoard;
  }
  
  /**
   * Sets the dashBoard property to this instance.
   */
  public void setDashBoard(final byte[] dashBoard) {
    checkDisposed();
    this.dashBoard = dashBoard;
  }
  
  /**
   * @return Returns the favorites property or <code>null</code> if not present.
   */
  public byte[] getFavorites() {
    checkDisposed();
    return this.favorites;
  }
  
  /**
   * Sets the favorites property to this instance.
   */
  public void setFavorites(final byte[] favorites) {
    checkDisposed();
    this.favorites = favorites;
  }
  
  /**
   * @return Returns the filters property or <code>null</code> if not present.
   */
  public byte[] getFilters() {
    checkDisposed();
    return this.filters;
  }
  
  /**
   * Sets the filters property to this instance.
   */
  public void setFilters(final byte[] filters) {
    checkDisposed();
    this.filters = filters;
  }
  
  @PreUpdate
  public void preUpdate() {
    if ((this.locked && (!this.enabled))) {
      this.locked = false;
      this.failedAttempt = 00;
    }
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserAccount other = (UserAccount) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
