package org.eclipse.osbp.authentication.account.entities;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.osbp.authentication.account.entities.UserGroup;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.Dispose;

@Entity
@Table(name = "FILTER")
@DiscriminatorValue(value = "FILTER")
@SuppressWarnings("all")
public class Filter implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "ID")
  private String id = java.util.UUID.randomUUID().toString();
  
  @Column(name = "FILTER")
  private String filter;
  
  @Column(name = "INVERS")
  private boolean invers;
  
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "USER_GROUP_ID")
  private UserGroup userGroup;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final String id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the filter property or <code>null</code> if not present.
   */
  public String getFilter() {
    checkDisposed();
    return this.filter;
  }
  
  /**
   * Sets the filter property to this instance.
   */
  public void setFilter(final String filter) {
    checkDisposed();
    this.filter = filter;
  }
  
  /**
   * @return Returns the invers property or <code>null</code> if not present.
   */
  public boolean getInvers() {
    checkDisposed();
    return this.invers;
  }
  
  /**
   * Sets the invers property to this instance.
   */
  public void setInvers(final boolean invers) {
    checkDisposed();
    this.invers = invers;
  }
  
  /**
   * @return Returns the userGroup property or <code>null</code> if not present.
   */
  public UserGroup getUserGroup() {
    checkDisposed();
    return this.userGroup;
  }
  
  /**
   * Sets the userGroup property to this instance.
   * Since the reference is a container reference, the opposite reference (UserGroup.userGroupFilter)
   * of the userGroup will be handled automatically and no further coding is required to keep them in sync.
   * See {@link UserGroup#setUserGroupFilter(UserGroup)}.
   */
  public void setUserGroup(final UserGroup userGroup) {
    checkDisposed();
    if (this.userGroup != null) {
      this.userGroup.internalRemoveFromUserGroupFilter(this);
    }
    internalSetUserGroup(userGroup);
    if (this.userGroup != null) {
      this.userGroup.internalAddToUserGroupFilter(this);
    }
    
  }
  
  /**
   * For internal use only!
   */
  public void internalSetUserGroup(final UserGroup userGroup) {
    this.userGroup = userGroup;
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Filter other = (Filter) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    
  }
}
