package org.eclipse.osbp.authentication.account.entities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PreRemove;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import org.eclipse.osbp.authentication.account.entities.Filter;
import org.eclipse.osbp.authentication.account.entities.UserAccount;
import org.eclipse.osbp.dsl.common.datatypes.IEntity;
import org.eclipse.osbp.runtime.common.annotations.AsTable;
import org.eclipse.osbp.runtime.common.annotations.Dispose;
import org.eclipse.osbp.runtime.common.annotations.DomainKey;
import org.eclipse.osbp.runtime.common.annotations.Properties;
import org.eclipse.osbp.runtime.common.annotations.Property;
import org.eclipse.osbp.runtime.common.annotations.UIGroup;
import org.eclipse.osbp.runtime.common.annotations.UniqueEntry;
import org.eclipse.osbp.runtime.common.validation.ErrorSeverity;

@Cacheable(false)
@Entity
@Table(name = "USER_GROUP", indexes = @Index(name = "USER_GROUP_GROUP_NAME", unique = true, columnList = "USER_GROUP_NAME"))
@DiscriminatorValue(value = "USER_GROUP")
@SuppressWarnings("all")
public class UserGroup implements IEntity {
  @Transient
  @Dispose
  private boolean disposed;
  
  @Id
  @Column(name = "ID")
  private String id = java.util.UUID.randomUUID().toString();
  
  @DomainKey
  @Column(name = "USER_GROUP_NAME")
  @UniqueEntry
  @UIGroup(name = "common")
  @NotNull(payload = ErrorSeverity.class)
  private String userGroupName;
  
  @Column(name = "POSITION")
  @UIGroup(name = "organization")
  @Properties(properties = @Property(key = "organization", value = ""))
  private String position;
  
  @Column(name = "DEFAULT_PERSPECTIVE")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "perspective", value = ""))
  private String defaultPerspective;
  
  @Column(name = "LOCALE_TAG")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "i18n", value = ""))
  private String localeTag;
  
  @Column(name = "THEME")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "theme", value = ""))
  private String theme;
  
  @Column(name = "PRINT_SERVICE")
  @UIGroup(name = "setting")
  @Properties(properties = @Property(key = "printservice", value = ""))
  private String printService;
  
  @JoinColumn(name = "USER_ACCOUNT_ID")
  @OneToMany(mappedBy = "userGroup")
  @AsTable
  private List<UserAccount> userAccount;
  
  @JoinColumn(name = "USER_GROUP_FILTER_ID")
  @OneToMany(mappedBy = "userGroup")
  private List<Filter> userGroupFilter;
  
  /**
   * @return true, if the object is disposed. 
   * Disposed means, that it is prepared for garbage collection and may not be used anymore. 
   * Accessing objects that are already disposed will cause runtime exceptions.
   * 
   */
  @Dispose
  public boolean isDisposed() {
    return this.disposed;
  }
  
  /**
   * Checks whether the object is disposed.
   * @throws RuntimeException if the object is disposed.
   */
  private void checkDisposed() {
    if (isDisposed()) {
      throw new RuntimeException("Object already disposed: " + this);
    }
  }
  
  /**
   * Calling dispose will destroy that instance. The internal state will be 
   * set to 'disposed' and methods of that object must not be used anymore. 
   * Each call will result in runtime exceptions.<br>
   * If this object keeps composition containments, these will be disposed too. 
   * So the whole composition containment tree will be disposed on calling this method.
   */
  @Dispose
  public void dispose() {
    if (isDisposed()) {
      return;
    }
    disposed = true;
  }
  
  /**
   * @return Returns the id property or <code>null</code> if not present.
   */
  public String getId() {
    checkDisposed();
    return this.id;
  }
  
  /**
   * Sets the id property to this instance.
   */
  public void setId(final String id) {
    checkDisposed();
    this.id = id;
  }
  
  /**
   * @return Returns the userGroupName property or <code>null</code> if not present.
   */
  public String getUserGroupName() {
    checkDisposed();
    return this.userGroupName;
  }
  
  /**
   * Sets the userGroupName property to this instance.
   */
  public void setUserGroupName(final String userGroupName) {
    checkDisposed();
    this.userGroupName = userGroupName;
  }
  
  /**
   * @return Returns the position property or <code>null</code> if not present.
   */
  public String getPosition() {
    checkDisposed();
    return this.position;
  }
  
  /**
   * Sets the position property to this instance.
   */
  public void setPosition(final String position) {
    checkDisposed();
    this.position = position;
  }
  
  /**
   * @return Returns the defaultPerspective property or <code>null</code> if not present.
   */
  public String getDefaultPerspective() {
    checkDisposed();
    return this.defaultPerspective;
  }
  
  /**
   * Sets the defaultPerspective property to this instance.
   */
  public void setDefaultPerspective(final String defaultPerspective) {
    checkDisposed();
    this.defaultPerspective = defaultPerspective;
  }
  
  /**
   * @return Returns the localeTag property or <code>null</code> if not present.
   */
  public String getLocaleTag() {
    checkDisposed();
    return this.localeTag;
  }
  
  /**
   * Sets the localeTag property to this instance.
   */
  public void setLocaleTag(final String localeTag) {
    checkDisposed();
    this.localeTag = localeTag;
  }
  
  /**
   * @return Returns the theme property or <code>null</code> if not present.
   */
  public String getTheme() {
    checkDisposed();
    return this.theme;
  }
  
  /**
   * Sets the theme property to this instance.
   */
  public void setTheme(final String theme) {
    checkDisposed();
    this.theme = theme;
  }
  
  /**
   * @return Returns the printService property or <code>null</code> if not present.
   */
  public String getPrintService() {
    checkDisposed();
    return this.printService;
  }
  
  /**
   * Sets the printService property to this instance.
   */
  public void setPrintService(final String printService) {
    checkDisposed();
    this.printService = printService;
  }
  
  /**
   * @return Returns an unmodifiable list of userAccount.
   */
  public List<UserAccount> getUserAccount() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetUserAccount());
  }
  
  /**
   * Sets the given userAccount to the object. Currently contained userAccount instances will be removed.
   * 
   * @param userAccount the list of new instances
   */
  public void setUserAccount(final List<UserAccount> userAccount) {
    // remove the old userAccount
    for(UserAccount oldElement : new ArrayList<UserAccount>(this.internalGetUserAccount())){
      removeFromUserAccount(oldElement);
    }
    
    // add the new userAccount
    for(UserAccount newElement : userAccount){
      addToUserAccount(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>UserAccount</code>s thereby lazy initializing it.
   */
  public List<UserAccount> internalGetUserAccount() {
    if (this.userAccount == null) {
      this.userAccount = new ArrayList<UserAccount>();
    }
    return this.userAccount;
  }
  
  /**
   * Adds the given userAccount to this object. <p>
   * Since the reference is a composition reference, the opposite reference (UserAccount.userGroup)
   * of the userAccount will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link UserAccount#setUserGroup(UserAccount)}.
   * 
   */
  public void addToUserAccount(final UserAccount userAccount) {
    checkDisposed();
    userAccount.setUserGroup(this);
  }
  
  /**
   * Removes the given userAccount from this object. <p>
   * 
   */
  public void removeFromUserAccount(final UserAccount userAccount) {
    checkDisposed();
    userAccount.setUserGroup(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToUserAccount(final UserAccount userAccount) {
    if(userAccount == null) {
    	return;
    }
    
    		internalGetUserAccount().add(userAccount);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromUserAccount(final UserAccount userAccount) {
    internalGetUserAccount().remove(userAccount);
  }
  
  /**
   * @return Returns an unmodifiable list of userGroupFilter.
   */
  public List<Filter> getUserGroupFilter() {
    checkDisposed();
    return Collections.unmodifiableList(internalGetUserGroupFilter());
  }
  
  /**
   * Sets the given userGroupFilter to the object. Currently contained userGroupFilter instances will be removed.
   * 
   * @param userGroupFilter the list of new instances
   */
  public void setUserGroupFilter(final List<Filter> userGroupFilter) {
    // remove the old filter
    for(Filter oldElement : new ArrayList<Filter>(this.internalGetUserGroupFilter())){
      removeFromUserGroupFilter(oldElement);
    }
    
    // add the new filter
    for(Filter newElement : userGroupFilter){
      addToUserGroupFilter(newElement);
    }
  }
  
  /**
   * For internal use only! Returns the list of <code>Filter</code>s thereby lazy initializing it.
   */
  public List<Filter> internalGetUserGroupFilter() {
    if (this.userGroupFilter == null) {
      this.userGroupFilter = new ArrayList<Filter>();
    }
    return this.userGroupFilter;
  }
  
  /**
   * Adds the given filter to this object. <p>
   * Since the reference is a composition reference, the opposite reference (Filter.userGroup)
   * of the filter will be handled automatically and no further coding is required to keep them in sync. 
   * See {@link Filter#setUserGroup(Filter)}.
   * 
   */
  public void addToUserGroupFilter(final Filter filter) {
    checkDisposed();
    filter.setUserGroup(this);
  }
  
  /**
   * Removes the given filter from this object. <p>
   * 
   */
  public void removeFromUserGroupFilter(final Filter filter) {
    checkDisposed();
    filter.setUserGroup(null);
  }
  
  /**
   * For internal use only!
   */
  public void internalAddToUserGroupFilter(final Filter filter) {
    if(filter == null) {
    	return;
    }
    
    		internalGetUserGroupFilter().add(filter);
  }
  
  /**
   * For internal use only!
   */
  public void internalRemoveFromUserGroupFilter(final Filter filter) {
    internalGetUserGroupFilter().remove(filter);
  }
  
  public boolean equalVersions(final Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    UserGroup other = (UserGroup) obj;
    if (this.id == null) {
      if (other.id != null)
        return false;
    } else if (!this.id.equals(other.id))
      return false;
    return true;
  }
  
  @Override
  public boolean equals(final Object obj) {
    return equalVersions(obj);
  }
  
  @Override
  public int hashCode() {
     int prime = 31;
    int result = 1;
    result = prime * result + ((this.id== null) ? 0 : this.id.hashCode());
    return result;
  }
  
  /**
   * Iterates all cross references and removes them from the parent to avoid ConstraintViolationException
   */
  @PreRemove
  protected void preRemove() {
    // remove the userAccount
    for(UserAccount oldElement : new ArrayList<UserAccount>(this.internalGetUserAccount())){
      removeFromUserAccount(oldElement);
    }
    // remove the userGroupFilter
    for(Filter oldElement : new ArrayList<Filter>(this.internalGetUserGroupFilter())){
      removeFromUserGroupFilter(oldElement);
    }
  }
}
