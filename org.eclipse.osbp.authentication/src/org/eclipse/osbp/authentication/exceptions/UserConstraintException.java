/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.exceptions;

import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * The Class UserConstraintException.
 */
public class UserConstraintException extends Exception {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * The Enum Violations.
	 */
	public static enum Violations {
				/** The username not unique. */
				USERNAME_NOT_UNIQUE,
				/** The email not unique. */
				EMAIL_NOT_UNIQUE,
				/** The unknown error. */
				UNKNOWN_ERROR}

    /** The constraints violations. */
    private Set<Violations> constraintsViolations ;

    /**
     * Instantiates a new user constraint exception.
     *
     * @param constraintsViolations the constraints violations
     */
    public UserConstraintException(Set<Violations> constraintsViolations) {
        this.constraintsViolations = constraintsViolations;
    }

    /**
     * Gets the constraints violations.
     *
     * @return the constraints violations
     */
    public Set<Violations> getConstraintsViolations() {
        return constraintsViolations;
    }

    /**
     * Sets the constraints violations.
     *
     * @param constraintsViolations the new constraints violations
     */
    public void setConstraintsViolations(Set<Violations> constraintsViolations) {
        this.constraintsViolations = constraintsViolations;
    }
}
