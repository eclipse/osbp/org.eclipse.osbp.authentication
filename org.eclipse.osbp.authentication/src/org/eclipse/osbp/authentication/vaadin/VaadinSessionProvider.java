/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.vaadin;

import com.vaadin.server.VaadinSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * depends on ideas of David Sowerby.
 */
public class VaadinSessionProvider {
    
    /** The log. */
    private static Logger log = LoggerFactory.getLogger(VaadinSessionProvider.class);

    /**
     * Gets the.
     *
     * @return the vaadin session
     */
    public VaadinSession get() {
        VaadinSession session = VaadinSession.getCurrent();

        // This may happen in background threads, or testing
        if (session == null) {
            String msg = "Vaadin session not present.  If you are testing, use a Mock for this provider";
            log.warn(msg);
            throw new IllegalStateException(msg);
        }

        return session;
    }

}
