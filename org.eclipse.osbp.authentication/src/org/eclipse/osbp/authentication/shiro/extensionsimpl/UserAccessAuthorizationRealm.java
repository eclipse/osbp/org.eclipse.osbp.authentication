/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.shiro.extensionsimpl;

import java.util.Collections;
import java.util.List;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.account.dtos.UserGroupDto;
import org.eclipse.osbp.authentication.providerimpl.UserAccessService;
import org.eclipse.osbp.authentication.shiro.extensions.IUserAccess;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization;
import org.eclipse.osbp.ui.api.useraccess.AbstractPosition;
import org.eclipse.osbp.ui.api.useraccess.IOrganizationService;
import org.eclipse.osbp.ui.api.useraccess.IPosition;
import org.eclipse.osbp.ui.api.useraccess.ISubOrganization;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserAccessAuthorizationRealm.
 * 
 * some useful hints upon integration vaadin / shiro
 * https://github.com/davidsowerby/krail/tree/master/src/main/java/uk/q3c
 * https://vaadin.com/forum/#!/thread/2009907/3349290
 * https://vaadin.com/forum#!/thread/8644145
 * http://mikepilone.blogspot.de/2013/07/vaadin-shiro-and-push.html
 * https://github.com/vaadin-kim/shiro-example
 */
public abstract class UserAccessAuthorizationRealm extends AuthorizingRealm implements IUserAccess {

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserAccessAuthorizationRealm.class);

	/** The portal id. */
	private String portalId = "";

	/**
	 * Instantiates a new user access authorization realm.
	 */
	public UserAccessAuthorizationRealm() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#
	 * findPositionForUser(java.lang.String)
	 */
	@Override
	public AbstractPosition findPositionForUser(String username) {
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			if( user.getPosition() != null && !user.getPosition().isEmpty() )
				return findPositionForPositionName(user.getPosition());

			UserGroupDto userGroup = user.getUserGroup();
			if (userGroup != null)
				return findPositionForPositionName(userGroup.getPosition());

		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#
	 * findPositionForPositionName(java.lang.String)
	 */
	@Override
	public AbstractPosition findPositionForPositionName(String positionName) {
		if (positionName != null) {
			String organizationID = ProductConfiguration.getAuthenticationOrganizationId();
			boolean organizationFound = false;
			boolean organizationDefined = false;
			for (IOrganizationService organizationService : UserAccessService.getOrganizationServices()) {
				organizationDefined = !organizationService.getOrganizationNames().isEmpty() || organizationDefined;
				ISubOrganization organization = organizationService.getOrganization(organizationID);
				if (organization != null) {
					organizationFound = true;
					IPosition position = organization.getPositionByLinkAlias(positionName);
					if (position instanceof AbstractPosition) {
						return (AbstractPosition) position;
					}
				}
			}
			// we found an authorizing organization but the role was not inside
			if(organizationFound) {
				throw new AccountException("The position "+positionName+" is not supported by the authorizing organization "+organizationID);
			}
			// no authorization service present - this probably is a osbp installation
			if(UserAccessService.getOrganizationServices().isEmpty() || !organizationDefined) {
				return null;
			}
		}
		if(!UserAccessService.getOrganizationServices().isEmpty()) {
			throw new AccountException("The user has no position assigned and cannot be authorized therefore");
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#findRolesForUser
	 * (java.lang.String)
	 */
	@Override
	public List<String> findRolesForUser(String username) {
		AbstractPosition position = findPositionForUser(username);
		if (position != null) {
			return position.getRoles();
		}
		return Collections.emptyList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#
	 * findPermissionsForUser(java.lang.String)
	 */
	@Override
	public AbstractAuthorization findPermissionsForUser(String username) {
		AbstractPosition position = findPositionForUser(username);
		if (position != null) {
			return position.getAuthorization();
		}
		return null;
	}

	@Override
	public AbstractAuthorization findPermissionsForAdministrator() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#getPortalId
	 * ()
	 */
	@Override
	public String getPortalId() {
		return portalId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#setPortalId
	 * (java.lang.String)
	 */
	@Override
	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.shiro.realm.AuthorizingRealm#doGetAuthorizationInfo(org.apache
	 * .shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.apache.shiro.realm.AuthenticatingRealm#doGetAuthenticationInfo(org
	 * .apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#
	 * checkUsernameUniqueness(java.lang.String)
	 */
	@Override
	public boolean checkUsernameExists(String username) {
		return getAllUsers().contains(username);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#
	 * checkEmailUniqueness(java.lang.String)
	 */
	@Override
	public boolean checkEmailExists(String email) {
		return getAllEmails().contains(email);
	}
}
