/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.shiro.extensionsimpl;

import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.realm.AuthenticatingRealm;

import org.eclipse.osbp.authentication.providerimpl.UserProtocol;
import org.eclipse.osbp.authentication.shiro.extensions.IPortalAuthenticationToken;

/**
 * The Class PortalUsernamePasswordToken.
 */
public class PortalUsernamePasswordToken extends UsernamePasswordToken implements IPortalAuthenticationToken {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 3390075289568367652L;
	
	/** The authenticating realm. */
	private AuthenticatingRealm fAuthenticatingRealm;
	
	/** The protocol. */
	private UserProtocol protocol;
	
	/**
	 * Instantiates a new portal username password token.
	 */
	public PortalUsernamePasswordToken() {
		super();
		portalId = "";
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param rememberMe the remember me
	 * @param host the host
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, char[] password, boolean rememberMe, String host) {
		super(username, password, rememberMe, host);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param rememberMe the remember me
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, char[] password,
			boolean rememberMe) {
		super(username, password, rememberMe);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param host the host
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, char[] password,
			String host) {
		super(username, password, host);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, char[] password) {
		super(username, password);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param rememberMe the remember me
	 * @param host the host
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, String password,
			boolean rememberMe, String host) {
		super(username, password, rememberMe, host);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param rememberMe the remember me
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, String password,
			boolean rememberMe) {
		super(username, password, rememberMe);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 * @param host the host
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, String password,
			String host) {
		super(username, password, host);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/**
	 * Instantiates a new portal username password token.
	 *
	 * @param clientHost the client host
	 * @param portalId the portal id
	 * @param username the username
	 * @param password the password
	 */
	public PortalUsernamePasswordToken(String clientHost, String portalId, String username, String password) {
		super(username, password);
		this.portalId = portalId;
		setHost(clientHost);
	}

	/** The portal id. */
	private String portalId; //add getter/setter

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IPortalAuthenticationToken#getPortalId()
	 */
	public String getPortalId() {
		return portalId;
	}

	/**
	 * Sets the portal id.
	 *
	 * @param portalId the new portal id
	 */
	public void setPortalId(String portalId) {
		this.portalId = portalId;
	}

	/**
	 * Gets the authenticated by realm.
	 *
	 * @return the authenticated by realm
	 */
	public AuthenticatingRealm getAuthenticatedByRealm() {
		return fAuthenticatingRealm;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IPortalAuthenticationToken#setAuthenticatedByRealm(org.apache.shiro.realm.AuthenticatingRealm)
	 */
	@Override
	public void setAuthenticatedByRealm(AuthenticatingRealm authenticatingRealm) {
		fAuthenticatingRealm = authenticatingRealm;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IPortalAuthenticationToken#setUserProtocol(org.eclipse.osbp.authentication.providerimpl.UserProtocol)
	 */
	@Override
	public void setUserProtocol(UserProtocol protocol) {
		this.protocol = protocol;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IPortalAuthenticationToken#getUserProtocol()
	 */
	@Override
	public UserProtocol getUserProtocol() {
		return protocol;
	}
}
