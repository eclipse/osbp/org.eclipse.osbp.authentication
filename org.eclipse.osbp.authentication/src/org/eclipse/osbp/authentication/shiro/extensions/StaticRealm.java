/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.shiro.extensions;

import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.authentication.providerimpl.AuthenticationInformation;
import org.eclipse.osbp.authentication.providerimpl.AuthorizationInformation;
import org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization;
import org.eclipse.osbp.ui.api.useraccess.AbstractPosition;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class StaticRealm.
 */
public class StaticRealm extends UserAccessAuthorizationRealm {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("realm");
	
	/** The static token. */
	private IPortalAuthenticationToken staticToken = null;

	/**
	 * Instantiates a new static realm.
	 */
	public StaticRealm() {
		setName(ProductConfiguration.SHIRO_STATIC_REALM); 		// This name must match the name in the User class's getPrincipals() method
		setCredentialsMatcher(new PasswordMatcher());
		setAuthorizationCachingEnabled(true); 
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#doGetAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        
        final String username = (String) getAvailablePrincipal(principals);
		UserAccountDto account = findUserAccount(username);
		if (account == null) {
			return null;
		}

		// the positions are linked by the username
		AbstractPosition position = findPositionForUser(account.getUserName()); 
		if(position == null) {
			return null;
		}
		return new AuthorizationInformation(
			getPortalId(),
			principals, 
			position, 
			findPermissionsForUser(username));
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) {
        staticToken = (IPortalAuthenticationToken)token;
		if (!(token instanceof UsernamePasswordToken)) {
			throw new IllegalStateException("Token has to be instance of UsernamePasswordToken class");
		}

		final UsernamePasswordToken userPassToken = (UsernamePasswordToken) token;

		if (userPassToken.getUsername() == null) {
			throw new AccountException("Null usernames are not allowed by this realm.");
		}

		UserAccountDto account = findUserAccount(userPassToken.getUsername());
		if (account == null)
			throw new UnknownAccountException();
		
		final AuthenticationInformation authenticationInfo = new AuthenticationInformation( account.getUserName(), 
																							account.getPassword(), 
																							ProductConfiguration.SHIRO_STATIC_REALM, 
																							account.getEnabled(), 
																							account.getLocked()); 
        if ( authenticationInfo.isLocked() ) {
            LOGGER.debug("Account {} is locked.", token);
            throw new LockedAccountException();
        }

        if ( ! authenticationInfo.isEnabled() ) {
            LOGGER.debug("Account {} is disabled.", token);
            throw new DisabledAccountException();
        }

    	staticToken.setAuthenticatedByRealm(this);

		return authenticationInfo;
	}

	/* (non-Javadoc)
	 * @see org.apache.shiro.realm.AuthenticatingRealm#supports(org.apache.shiro.authc.AuthenticationToken)
	 */
	@Override
    public boolean supports(AuthenticationToken token) {
        return token instanceof IPortalAuthenticationToken &&
            getPortalId().equals(((IPortalAuthenticationToken)token).getPortalId());
    }

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensions.IUserAccess#getAllEmails()
	 */
	@Override
	public Set<String> getAllEmails() {
		Set<String> retcode = new HashSet<>();
		retcode.add(ProductConfiguration.getAdminEmail());
		return retcode;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#getAllUsers()
	 */
	@Override
	public Set<String> getAllUsers() {
		Set<String> retcode = new HashSet<>();
		retcode.add(IUserAccessService.NAME_ADMINISTRATOR);
		return retcode;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#getAllUsersPositions()
	 */
	@Override
	public Map<String, String> getAllUsersPositions() {
		Map<String,String> retcode = new HashMap<>();
		retcode.put(IUserAccessService.NAME_ADMINISTRATOR, IUserAccessService.PASSWORD_ADMINISTRATOR);
		return retcode;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#findUserAccount(java.lang.String)
	 */
	@Override
	public UserAccountDto findUserAccount(String username) {
		if(!IUserAccessService.NAME_ADMINISTRATOR.equals(username)) {
			return null;
		}
		UserAccountDto user = new UserAccountDto();
		user.setEmail(ProductConfiguration.getAdminEmail());
		user.setEnabled(true);
		user.setLocked(false);
		user.setPosition(IUserAccessService.POS_ADMINISTRATOR);
		user.setUserName(IUserAccessService.NAME_ADMINISTRATOR);
		user.setPassword(encryptPassword(generatePassword()));
		return user;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#findPositionForUser(java.lang.String)
	 */
	@Override
	public AbstractPosition findPositionForUser(String username) {
		return findPositionForPositionName(IUserAccessService.POS_ADMINISTRATOR);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.osbp.authentication.shiro.extensionsimpl.UserAccessAuthorizationRealm#findUsersForPosition(java.lang.String)
	 */
	@Override
	public Set<String> findUsersForPosition(String orgNode) {
		Set<String> retcode = new HashSet<>();
		retcode.add(IUserAccessService.NAME_ADMINISTRATOR);
		return retcode;
	}

	/**
	 * Generate password.
	 *
	 * @return the string
	 */
	private String generatePassword() {
		Calendar cal = Calendar.getInstance();
		Integer number = cal.get(Calendar.DATE)*100;
		number += cal.get(Calendar.MONTH);
		number ++; // month is idiotically 0 based
		number += cal.get(Calendar.YEAR);
		number += cal.get(Calendar.HOUR_OF_DAY)*100;
		return number.toString();
	}
	
	/**
	 * Encrypt password.
	 *
	 * @param decryptedPassword the decrypted password
	 * @return the string
	 */
	private String encryptPassword(String decryptedPassword) {
		return staticToken.getUserProtocol().encryptPassword(decryptedPassword);
	}
	
	@Override
	public AbstractAuthorization findPermissionsForAdministrator() {
		return null;
	}
}
