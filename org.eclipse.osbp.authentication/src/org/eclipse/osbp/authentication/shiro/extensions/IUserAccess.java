/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.shiro.extensions;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization;
import org.eclipse.osbp.ui.api.useraccess.AbstractPosition;

/**
 * The Interface IUserAccess.
 */
public interface IUserAccess {
	
	/**
	 * Gets the portal id.
	 *
	 * @return the portal id
	 */
	String getPortalId();
	
	/**
	 * Sets the portal id.
	 *
	 * @param portalId the new portal id
	 */
	void setPortalId(String portalId);
	
	/**
	 * Gets the all users.
	 *
	 * @return the all users
	 */
	Set<String> getAllUsers();
	
	/**
	 * Gets the all emails.
	 *
	 * @return the all emails
	 */
	Set<String> getAllEmails();
	
	/**
	 * Find user account.
	 *
	 * @param username the username
	 * @return the user account dto
	 */
	UserAccountDto findUserAccount(String username);
	
	/**
	 * Find users for position.
	 *
	 * @param orgNode the org node
	 * @return the sets the
	 */
	Set<String> findUsersForPosition(String orgNode);
	
	/**
	 * Find position for user.
	 *
	 * @param username the username
	 * @return the a position
	 */
	AbstractPosition findPositionForUser(String username);
	
	/**
	 * Find roles for user.
	 *
	 * @param username the username
	 * @return the collection
	 */
	Collection<String> findRolesForUser(String username);
	
	/**
	 * Find permissions for user.
	 *
	 * @param username the username
	 * @return the permission list
	 */
	AbstractAuthorization findPermissionsForUser(String username);
	
	/**
	 * Find permissions for administrator.
	 *
	 * @return the abstract authorization
	 */
	AbstractAuthorization findPermissionsForAdministrator();
	
	/**
	 * Check username uniqueness.
	 *
	 * @param username the username
	 * @return true, if successful
	 */
	boolean checkUsernameExists(String username);
	
	/**
	 * Check email uniqueness.
	 *
	 * @param email the email
	 * @return true, if successful
	 */
	boolean checkEmailExists(String email);
	
	/**
	 * Gets the all users positions.
	 *
	 * @return the all users positions
	 */
	Map<String, String> getAllUsersPositions();
	
	/**
	 * Find position for position name.
	 *
	 * @param positionName the position name
	 * @return the a position
	 */
	AbstractPosition findPositionForPositionName(String positionName);
}
