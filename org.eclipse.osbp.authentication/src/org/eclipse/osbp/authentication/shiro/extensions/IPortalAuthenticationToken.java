/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.shiro.extensions;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.realm.AuthenticatingRealm;

import org.eclipse.osbp.authentication.providerimpl.UserProtocol;

/**
 * The Interface IPortalAuthenticationToken.
 */
public interface IPortalAuthenticationToken extends AuthenticationToken {
    
    /**
     * Gets the portal id.
     *
     * @return the portal id
     */
    String getPortalId();
	
	/**
	 * Sets the authenticated by realm.
	 *
	 * @param authenticatingRealm the new authenticated by realm
	 */
	void setAuthenticatedByRealm(AuthenticatingRealm authenticatingRealm);
	
	/**
	 * Sets the user protocol.
	 *
	 * @param protocol the new user protocol
	 */
	void setUserProtocol(UserProtocol protocol);
	
	/**
	 * Gets the user protocol.
	 *
	 * @return the user protocol
	 */
	UserProtocol getUserProtocol();
}
