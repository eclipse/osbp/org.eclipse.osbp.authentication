/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.providerimpl;

import java.util.HashSet;

import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;
import org.eclipse.osbp.ui.api.useraccess.AbstractAuthorization;
import org.eclipse.osbp.ui.api.useraccess.AbstractPosition;

/**
 * The Class AuthorizationInformation holds metadata of the authenticated subject.
 */
public class AuthorizationInformation extends SimpleAuthorizationInfo {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 8654353182167477248L;
	
	/** The position. */
	private final transient AbstractPosition fPosition;
	
	/** The portal id. */
	private final String fPortalId;
	
	/** The principal. */
	private final PrincipalCollection fPrincipal;
	
	/**
	 * Instantiates a new authorization information.
	 *
	 * @param portalId the portal id
	 * @param principals the principals
	 * @param position the position
	 * @param roles the roles
	 * @param permissions the permissions
	 */
	public AuthorizationInformation(String portalId, PrincipalCollection principals, AbstractPosition position, AbstractAuthorization authorization) {
		super();
		fPortalId = portalId;
		fPrincipal = principals;
		fPosition = position;
		if(roles != null) {
			super.setRoles(new HashSet<>(authorization.getRoles()));
		}
	}
	
	/**
	 * Gets the portal id.
	 *
	 * @return the portal id
	 */
	public String getPortalId() {
		return fPortalId;
	}
	
	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return fPrincipal.getPrimaryPrincipal().toString();
	}
	
	/**
	 * Gets the position.
	 *
	 * @return the position
	 */
	public AbstractPosition getPosition() {
		return fPosition;
	}

}
