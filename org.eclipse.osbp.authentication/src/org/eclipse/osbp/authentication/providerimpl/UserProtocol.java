/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.providerimpl;

import java.util.Collection;

import org.apache.shiro.authc.credential.PasswordService;
import org.eclipse.osbp.authentication.account.dtos.UserAccountDto;
import org.eclipse.osbp.core.api.persistence.IPersistenceService;
import org.eclipse.osbp.jpa.services.Query;
import org.eclipse.osbp.jpa.services.filters.LCompare;
import org.eclipse.osbp.preferences.ProductConfiguration;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.runtime.common.filter.IDTOService;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserProtocol controls login attempts of users, if successful or
 * not. User accounts will be locked on too many unsuccessful login attempts.
 * The maximum tries is configured in the preferences.
 */
public class UserProtocol {

	/** The dto user account dto service. */
	protected static IDTOService<UserAccountDto> dtoUserAccountDtoService = null;

	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProtocol.class);

	/** The persistence service. */
	protected static IPersistenceService persistenceService;

	/** The persistence id. */
	protected static String persistenceId = "authentication";

	/** The password service. */
	protected static PasswordService passwordService = null;

	//
	// OSGI Infrastructure
	//

	/**
	 * Called by OSGi-DS to activate the service.
	 *
	 * @param context
	 *            the context
	 * @throws Exception
	 *             the exception
	 */
	protected void activate(ComponentContext context) {		// NOSONAR
		LOGGER.debug("UserProtocol activated");
	}

	/**
	 * Called by OSGi-DS to deactivate the service.
	 *
	 * @param context
	 *            the context
	 */
	protected void deactivate(ComponentContext context) {
		// NOP
	}

	/**
	 * Gets the dto user account dto service.
	 *
	 * @return the dto user account dto service
	 */
	public static IDTOService<UserAccountDto> getDtoUserAccountDtoService() {
		return UserProtocol.dtoUserAccountDtoService;
	}

	/**
	 * Gets the password service.
	 *
	 * @return the password service
	 */
	public static PasswordService getPasswordService() {
		return UserProtocol.passwordService;
	}

	/**
	 * Find user account.
	 *
	 * @param username
	 *            the username
	 * @return the user account dto
	 */
	public UserAccountDto findUserAccount(String username) {
		Collection<UserAccountDto> users = null;
			if (ProductConfiguration.getIdentifyByUsername()) {
				try {
				// try to find by username first
				users = getDtoUserAccountDtoService().find(new Query(new LCompare.Equal("userName", username)));
				} catch (Exception e) {
					LOGGER.error("Exception while trying to find user {} by userName", username, e);
				}
			} else {
				try {
				// try to find by email
				users = getDtoUserAccountDtoService().find(new Query(new LCompare.Equal("email", username)));
				} catch (Exception e) {
					LOGGER.error("Exception while trying to find user {} by email", username, e);
				}
			}
		if (users != null) {
			if (users.size() > 1) {
				LOGGER.error("user account is not unique - first match taken");
			} else if (users.isEmpty()) {
				LOGGER.debug("user account not found");
				return null;
			}
			return users.iterator().next();
		}
		return null;
	}

	/**
	 * Update user account.
	 *
	 * @param user
	 *            the user
	 */
	public void updateUserAccount(UserAccountDto user) {
		getDtoUserAccountDtoService().update(user);
	}

	/**
	 * Delete user account.
	 *
	 * @param user
	 *            the user
	 */
	public void deleteUserAccount(UserAccountDto user) {
		getDtoUserAccountDtoService().delete(user);
	}

	/**
	 * Track successful login attempt.
	 *
	 * @param username
	 *            the username
	 */
	public void trackSuccessfulLoginAttempt(String username) {
		UserAccountDto user = this.findUserAccount(username);
		if (user != null) {
			int cnt = user.getSuccessfulAttempt();
			cnt++;
			user.setSuccessfulAttempt(cnt);
			user.setFailedAttempt(0);
			updateUserAccount(user);
		}
	}

	/**
	 * Track failed login attempt.
	 *
	 * @param username
	 *            the username
	 * @return the int
	 */
	public int trackFailedLoginAttempt(String username) {
		if (IUserAccessService.NAME_ADMINISTRATOR.equals(username)) {
			return 0;
		}
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			int cnt = user.getFailedAttempt();
			cnt++;
			user.setFailedAttempt(cnt);
			updateUserAccount(user);
			return cnt;
		}
		return 0;
	}

	/**
	 * Checks if is account locked.
	 *
	 * @param username
	 *            the username
	 * @return true, if is account locked
	 */
	public boolean isAccountLocked(String username) {
		if (IUserAccessService.NAME_ADMINISTRATOR.equals(username)) {
			return false;
		}
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			return user.getLocked();
		}
		return false;
	}

	/**
	 * Checks if the account is still not registered.
	 *
	 * @param username
	 *            the username
	 * @return true, if the account is still not registered
	 */
	public boolean isAccountNotRegistered(String username) {
		if (IUserAccessService.NAME_ADMINISTRATOR.equals(username)) {
			return false;
		}
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			return user.getNotRegistered();
		}
		return true;	// if no account information present - it is the same as not registered
	}

	/**
	 * Checks if is account enabled.
	 *
	 * @param username
	 *            the username
	 * @return true, if is account enabled
	 */
	public boolean isAccountEnabled(String username) {
		if (IUserAccessService.NAME_ADMINISTRATOR.equals(username)) {
			return true;
		}
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			return user.getEnabled();
		}
		return false;
	}

	/**
	 * Lock account.
	 *
	 * @param username
	 *            the username
	 * @param locked
	 *            the locked
	 */
	public void lockAccount(String username, boolean locked) {
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			user.setLocked(locked);
			updateUserAccount(user);
		}
	}

	/**
	 * Enable account.
	 *
	 * @param username
	 *            the username
	 * @param enabled
	 *            the enabled
	 */
	public void enableAccount(String username, boolean enabled) {
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			user.setEnabled(enabled);
			updateUserAccount(user);
		}
	}

	/**
	 * Sets the cookie hash.
	 *
	 * @param username
	 *            the username
	 * @param cookie
	 *            the cookie
	 */
	public void setCookieHash(String username, String cookie) {
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			user.setCookieHashCode(hashCodeForCookie(cookie));
			updateUserAccount(user);
		}
	}

	/**
	 * Hash code for cookie.
	 *
	 * @param cookie
	 *            the cookie
	 * @return the int
	 */
	private int hashCodeForCookie(String cookie) {
		return cookie == null ? 0 : cookie.hashCode();
	}

	/**
	 * Checks if is cookie valid.
	 *
	 * @param username
	 *            the username
	 * @param cookie
	 *            the cookie
	 * @return true, if is cookie valid
	 */
	public boolean isCookieValid(String username, String cookie) {
		UserAccountDto user = findUserAccount(username);
		if (user != null) {
			if (Integer.compareUnsigned(user.getCookieHashCode(), hashCodeForCookie(cookie)) == 0) {
				LOGGER.debug("cookie is valid");
				return true;
			} else {
				int hashCode = 0;
				if(cookie != null) {
					hashCode = cookie.hashCode();
				}
				LOGGER.debug("fraud detected as an invalid cookie was presented. expected:{} found:{}",user.getCookieHashCode(), hashCode);
				return false;
			}
		} else {
			LOGGER.error("user '{}' could not be found", username);
			return true;
		}
	}

	/**
	 * Gets the persistence service.
	 *
	 * @return the persistence service
	 */
	public IPersistenceService getPersistenceService() {
		return persistenceService;
	}

	/**
	 * Gets the persistence id.
	 *
	 * @return the persistence id
	 */
	public static String getPersistenceId() {
		return persistenceId;
	}

	/**
	 * Encrypt password.
	 *
	 * @param decryptedPassword
	 *            the decrypted password
	 * @return the string
	 */
	public String encryptPassword(String decryptedPassword) {
		return UserProtocol.passwordService.encryptPassword(decryptedPassword);
	}

	/**
	 * Encrypt passwords.
	 */
	public void encryptPasswords() {
		Query query = new Query(new LCompare.Equal("enabled", 0));
		int size = getDtoUserAccountDtoService().size(query);
		Collection<UserAccountDto> users = getDtoUserAccountDtoService().getByIndex(0, size, query);
		for (UserAccountDto user : users) {
			user.setPassword(encryptPassword(user.getPassword()));
			user.setEnabled(true);
			getDtoUserAccountDtoService().update(user);
		}
	}
}
