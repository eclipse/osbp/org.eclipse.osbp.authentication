/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication.providerimpl;

import org.apache.shiro.authc.SimpleAuthenticationInfo;

// TODO: Auto-generated Javadoc
/**
 * The Class AuthenticationInformation.
 */
public class AuthenticationInformation extends SimpleAuthenticationInfo {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6543741457178913445L;
	
	/** The enabled. */
	private boolean enabled;
	
	/** The locked. */
	private boolean locked;

	/**
	 * Instantiates a new authentication information.
	 *
	 * @param principal the principal
	 * @param credentials the credentials
	 * @param realmName the realm name
	 * @param enabled the enabled
	 * @param locked the locked
	 */
	public AuthenticationInformation(Object principal, Object credentials,
			String realmName, boolean enabled, boolean locked) {
		super(principal, credentials, realmName);
		this.enabled = enabled;
		this.locked = locked;
	}

	/**
	 * Checks if is enabled.
	 *
	 * @return true, if is enabled
	 */
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * Sets the enabled.
	 *
	 * @param enabled the new enabled
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/**
	 * Checks if is locked.
	 *
	 * @return true, if is locked
	 */
	public boolean isLocked() {
		return locked;
	}

	/**
	 * Sets the locked.
	 *
	 * @param locked the new locked
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}
