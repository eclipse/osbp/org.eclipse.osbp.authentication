/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.authentication;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.osbp.authentication.account.entities.UserAccount;
import org.eclipse.osbp.gitinfo.Loginfo;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent;
import org.eclipse.osbp.runtime.common.event.IEventDispatcher;
import org.eclipse.osbp.runtime.common.event.EventDispatcherEvent.EventDispatcherCommand;
import org.eclipse.osbp.ui.api.complexdatacontainer.IComplexDataContainerChangedListener;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The Class Activator.
 * 
 */
public class Activator implements BundleActivator, IEventDispatcher.Receiver {

	/** The context. */
	private static BundleContext context;

	private IEventDispatcher eventDispatcher;

	private ServiceTracker<IEventDispatcher, IEventDispatcher> eventDispatcherTracker;

	/** The listeners which are listening for changed complex data changes. */
	public static final List<IComplexDataContainerChangedListener> userAccountChangedListeners = new ArrayList<>();

	/**
	 * Gets the context.
	 *
	 * @return the context
	 */
	static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext
	 * )
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Loginfo li = new Loginfo();
		li.print(Activator.class.getCanonicalName(), Activator.class.getClassLoader());
		Activator.context = bundleContext;
		eventDispatcherTracker = new ServiceTracker<IEventDispatcher, IEventDispatcher>(context, IEventDispatcher.class,
				null) {
			@Override
			public IEventDispatcher addingService(ServiceReference<IEventDispatcher> reference) {
				eventDispatcher = context.getService(reference);
				if (eventDispatcher != null) {
					eventDispatcher.addEventReceiver(Activator.this);
				}

				return null;
			}

			@Override
			public void removedService(ServiceReference<IEventDispatcher> reference, IEventDispatcher service) {
				eventDispatcher.removeEventReceiver(Activator.this);
				eventDispatcher = null;
				super.removedService(reference, service);
			}
		};
		eventDispatcherTracker.open();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		if (eventDispatcherTracker != null) {
			eventDispatcher.removeEventReceiver(this);
			eventDispatcher = null;
			eventDispatcherTracker.close();
		}
		Activator.context = null;
	}

	@Override
	public void receiveEvent(EventDispatcherEvent event) {
		if ((event.getCommand() == EventDispatcherCommand.REFRESH
				|| event.getCommand() == EventDispatcherCommand.DELETE)
				&& event.getTopic().equals(UserAccount.class.getName())) {
			for (IComplexDataContainerChangedListener container : userAccountChangedListeners) {
				container.complexDataContainerChanged();
			}
		}
	}

}
