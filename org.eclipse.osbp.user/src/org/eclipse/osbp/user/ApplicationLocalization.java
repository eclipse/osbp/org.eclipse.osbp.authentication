/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.user;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MLocalizable;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;

public class ApplicationLocalization {
	/**
	 * Updates the default locale for all children of the given E4 window.
	 *
	 * @param app the app
	 */
	public static void updateLocalization(MApplication app) {
		traverseLocalizationChildren(app);
	}

	// update localization for all children of MWindow
	protected static void traverseLocalizationChildren(MElementContainer<?> parent) {
		for(MUIElement element:parent.getChildren()) {
			if(element.isToBeRendered()) {
				if(element instanceof MLocalizable) {
					((MLocalizable)element).updateLocalization();
				}
				if(element instanceof MElementContainer<?>) {
					traverseLocalizationChildren((MElementContainer<?>) element);
				}
				if(element instanceof MPart) {
					if(((MPart)element).getToolbar() != null) {
						((MPart)element).getToolbar().updateLocalization();
						traverseLocalizationChildren(((MPart)element).getToolbar());
					}
				}
			}
		}
	}
}
