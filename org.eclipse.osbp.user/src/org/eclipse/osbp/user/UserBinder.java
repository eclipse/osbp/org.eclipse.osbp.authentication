/**
 *                                                                            
 * Copyright (c) 2011, 2016 - Loetz GmbH&Co.KG (69115 Heidelberg, Germany)
 *                                                                            
 * All rights reserved. This program and the accompanying materials           
 * are made available under the terms of the Eclipse Public License 2.0        
 * which accompanies this distribution, and is available at                  
 * https://www.eclipse.org/legal/epl-2.0/                                 
 *                                 
 * SPDX-License-Identifier: EPL-2.0                                 
 *                                                                            
 * Contributors:   
 * Christophe Loetz (Loetz GmbH&Co.KG) - initial implementation 
 */
package org.eclipse.osbp.user;

import org.eclipse.osbp.ui.api.customfields.IBlobService;
import org.eclipse.osbp.ui.api.useraccess.IUserAccessService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UserBinder.
 */
@Component
public class UserBinder {
	
	/** The Constant LOGGER. */
	private static final Logger LOGGER = LoggerFactory.getLogger("servicebinder");
	
	/** The user access service. */
	private static IUserAccessService userAccessService;

	/**
	 * Gets the user access service.
	 *
	 * @return the user access service
	 */
	public static IUserAccessService getUserAccessService() {
		return userAccessService;
	}
	
	/**
	 * Bind user access service.
	 *
	 * @param userAccessService the user access service
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY, policy = ReferencePolicy.STATIC)
	public synchronized void bindUserAccessService(final IUserAccessService userAccessService) {
		UserBinder.userAccessService = userAccessService;
		LOGGER.debug("UserMenuAuthenticationService bound");
	}

	/**
	 * Unbind user access service.
	 *
	 * @param userAccessService the user access service
	 */
	public synchronized void unbindUserAccessService(final IUserAccessService userAccessService) {
		UserBinder.userAccessService = null;
		LOGGER.debug("UserMenuAuthenticationService unbound");
	}
}
